SET FOREIGN_KEY_CHECKS = 0; 
SET @tables = NULL;
SELECT GROUP_CONCAT(table_schema, '.', table_name) INTO @tables
  FROM information_schema.tables 
  WHERE table_schema = 'thearena';

SET @tables = CONCAT('DROP TABLE ', @tables);
PREPARE stmt FROM @tables;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
SET FOREIGN_KEY_CHECKS = 1; 

CREATE TABLE Division (DivisionId int(10) NOT NULL AUTO_INCREMENT, Name varchar(255), Game int(10) NOT NULL, Deleted bit(1), PRIMARY KEY (DivisionId)) CHARACTER SET UTF8;
CREATE TABLE Message (MessageId int(10) NOT NULL AUTO_INCREMENT, Content varchar(255), Time int(10) NOT NULL, Sender int(10) NOT NULL, Receiver int(10) NOT NULL, Deleted bit(1), PRIMARY KEY (MessageId)) CHARACTER SET UTF8;
CREATE TABLE Ban (BanId int(10) NOT NULL AUTO_INCREMENT, Commentary varchar(255), BannedGeek int(10) NOT NULL, BanPeriod int(10) NOT NULL, BanReason varchar(255) NOT NULL, Deleted bit(1), PRIMARY KEY (BanId)) CHARACTER SET UTF8;
CREATE TABLE Ranking (Standing int(10), Game int(10) NOT NULL, Division int(10), Team varchar(255) NOT NULL, Deleted bit(1), CONSTRAINT PK_Ranking PRIMARY KEY (Game, Team)) CHARACTER SET UTF8;
CREATE TABLE Tournament (TournamentId varchar(255) NOT NULL, Name varchar(255), Rules varchar(255), Slots int(10), PlayerNumber int(10), Tags varchar(255), RegisteringPeriod int(10) NOT NULL, PlayingPeriod int(10) NOT NULL, Game int(10) NOT NULL, Deleted bit(1), Organiser int(10) NOT NULL, PRIMARY KEY (TournamentId)) CHARACTER SET UTF8;
CREATE TABLE Game (GameId int(10) NOT NULL AUTO_INCREMENT, Name varchar(255), AgeLimit int(10), GameType varchar(255) NOT NULL, Deleted bit(1), PRIMARY KEY (GameId)) CHARACTER SET UTF8;
CREATE TABLE Team (TeamId varchar(255) NOT NULL, Name varchar(255), Tags varchar(255), Captain int(10) NOT NULL, Deleted bit(1), PRIMARY KEY (TeamId)) CHARACTER SET UTF8;
CREATE TABLE Period (PeriodId int(10) NOT NULL AUTO_INCREMENT, Start int(10), End int(10), Deleted bit(1), PRIMARY KEY (PeriodId)) CHARACTER SET UTF8;
CREATE TABLE Participation (Place int(10), Qualified bit(1), Team varchar(255) NOT NULL, Tournament varchar(255) NOT NULL, Deleted bit(1), CONSTRAINT PK_Participation PRIMARY KEY (Team, Tournament)) CHARACTER SET UTF8;
CREATE TABLE Versus (VersusId int(10) NOT NULL AUTO_INCREMENT, Team1 varchar(255), Team2 varchar(255), VersusPeriod int(10) NOT NULL, Result int(10) NOT NULL, Tournament varchar(255) NOT NULL, Deleted bit(1), PRIMARY KEY (VersusId)) CHARACTER SET UTF8;
CREATE TABLE Reason (ReasonId varchar(255) NOT NULL, Description varchar(255), Deleted bit(1), PRIMARY KEY (ReasonId)) CHARACTER SET UTF8;
CREATE TABLE Statistics (Game int(10) NOT NULL, Team varchar(255) NOT NULL, Player int(10) NOT NULL, WinNumber int(10), LoseNumber int(10), LifetimeScore int(10), Deleted bit(1), CONSTRAINT PK_Statistics PRIMARY KEY (Game, Team, Player)) CHARACTER SET UTF8;
CREATE TABLE Round (RoundId int(10) NOT NULL, Versus int(10) NOT NULL, Result int(10), Deleted bit(1), CONSTRAINT PK_Round PRIMARY KEY (Versus, RoundId)) CHARACTER SET UTF8;
CREATE TABLE Score (Result int(10) NOT NULL, ScoreType varchar(255) NOT NULL, Winner int(10), Loser int(10), Deleted bit(1), CONSTRAINT PK_Score PRIMARY KEY (Result, ScoreType)) CHARACTER SET UTF8;
CREATE TABLE GameType (GameTypeId varchar(255) NOT NULL, Name varchar(255), Description varchar(255), Deleted bit(1), PRIMARY KEY (GameTypeId)) CHARACTER SET UTF8;
CREATE TABLE ScoreType (ScoreTypeId varchar(255) NOT NULL, Name varchar(255), Deleted bit(1), PRIMARY KEY (ScoreTypeId)) CHARACTER SET UTF8;
CREATE TABLE Visitor (Ip varchar(255) NOT NULL, Deleted bit(1), PRIMARY KEY (Ip)) CHARACTER SET UTF8;
CREATE TABLE Geek (UserId int(10) NOT NULL AUTO_INCREMENT, Username varchar(255) NOT NULL, Name varchar(255), Surname varchar(255), Password varchar(255), Mail varchar(255), Birthdate int(10), Deleted bit(1), PRIMARY KEY (UserId)) CHARACTER SET UTF8;
CREATE TABLE Result (ResultId int(10) NOT NULL AUTO_INCREMENT, Loser varchar(255) NOT NULL, Winner varchar(255) NOT NULL, Deleted bit(1), PRIMARY KEY (ResultId)) CHARACTER SET UTF8;
CREATE TABLE Roles (RoleId varchar(255) NOT NULL, Deleted bit(1), PRIMARY KEY (RoleId)) CHARACTER SET UTF8;
CREATE TABLE Configuration (Geek int(10) NOT NULL, Parameter varchar(255) NOT NULL, value varchar(255), Deleted bit(1), CONSTRAINT PK_Configuration PRIMARY KEY (Geek, Parameter)) CHARACTER SET UTF8;
CREATE TABLE Parameter (ParameterId varchar(255), Preselected varchar(255), Deleted bit(1), PRIMARY KEY (ParameterId)) CHARACTER SET UTF8;
CREATE TABLE Roles_Geek (Role varchar(255) NOT NULL, Geek int(10) NOT NULL, Deleted bit(1), CONSTRAINT PK_Roles_Geek PRIMARY KEY (Role, Geek)) CHARACTER SET UTF8;
CREATE TABLE Team_Geek (Player int(10) NOT NULL, Team varchar(255) NOT NULL, Deleted bit(1), CONSTRAINT PK_Team_Geek PRIMARY KEY (Player, Team)) CHARACTER SET UTF8;
CREATE TABLE FollowTeam (Geek int(10) NOT NULL, Team varchar (255) NOT NULL, Deleted bit(1), CONSTRAINT PK_TeF PRIMARY KEY(Geek, Team)) CHARACTER SET UTF8;
CREATE TABLE FollowPlayer (Geek int(10) NOT NULL, Player int(10) NOT NULL, Deleted bit(1), CONSTRAINT PK_PF PRIMARY KEY(Geek, Player)) CHARACTER SET UTF8;
CREATE TABLE FollowGame (Geek int(10) NOT NULL, Game int(10) NOT NULL, Deleted bit(1), CONSTRAINT PK_GF PRIMARY KEY(Geek, Game)) CHARACTER SET UTF8;
CREATE TABLE FollowTournament (Geek int(10) NOT NULL, Tournament varchar (255) NOT NULL, Deleted bit(1), CONSTRAINT PK_ToF PRIMARY KEY(Geek, Tournament)) CHARACTER SET UTF8;
CREATE TABLE TournamentLog (EntryId int(10) NOT NULL AUTO_INCREMENT, Tournament varchar(255) NOT NULL, Entry varchar(255), Time int(10), Deleted bit(1), PRIMARY KEY (EntryId)) CHARACTER SET UTF8;
CREATE TABLE TournamentTag (TagId int(10) NOT NULL AUTO_INCREMENT, Tournament varchar(255) NOT NULL, Tag varchar(255), Deleted bit(1), PRIMARY KEY (TagId)) CHARACTER SET UTF8;
CREATE TABLE TeamTag (TagId int(10) NOT NULL AUTO_INCREMENT, Team varchar(255) NOT NULL, Tag varchar(255), Deleted bit(1), PRIMARY KEY (TagId)) CHARACTER SET UTF8; 

ALTER TABLE Division ADD CONSTRAINT FK_Division FOREIGN KEY (Game) REFERENCES Game (GameId);
ALTER TABLE Message ADD CONSTRAINT FK_Message1 FOREIGN KEY (Sender) REFERENCES Geek (UserId);
ALTER TABLE Message ADD CONSTRAINT FK_Message2 FOREIGN KEY (Receiver) REFERENCES Geek (UserId);
ALTER TABLE Ban ADD CONSTRAINT FK_Ban1 FOREIGN KEY (BannedGeek) REFERENCES Geek (UserId);
ALTER TABLE Ban ADD CONSTRAINT FK_Ban2 FOREIGN KEY (BanReason) REFERENCES Reason (ReasonId);
ALTER TABLE Ban ADD CONSTRAINT FK_Ban3 FOREIGN KEY (BanPeriod) REFERENCES Period (PeriodId);
ALTER TABLE Ranking ADD CONSTRAINT FK_Ranking1 FOREIGN KEY (Game) REFERENCES Game (GameId);
ALTER TABLE Ranking ADD CONSTRAINT FK_Ranking2 FOREIGN KEY (Team) REFERENCES Team (TeamId);
ALTER TABLE Tournament ADD CONSTRAINT FK_Tournament1 FOREIGN KEY (Organiser) REFERENCES Geek (UserId);
ALTER TABLE Tournament ADD CONSTRAINT FK_Tournament2 FOREIGN KEY (Game) REFERENCES Game (GameId);
ALTER TABLE Tournament ADD CONSTRAINT FK_Tournament3 FOREIGN KEY (RegisteringPeriod) REFERENCES Period (PeriodId);
ALTER TABLE Tournament ADD CONSTRAINT FK_Tournament4 FOREIGN KEY (PlayingPeriod) REFERENCES Period (PeriodId);
ALTER TABLE Game ADD CONSTRAINT FK_Game FOREIGN KEY (GameType) REFERENCES GameType (GameTypeId);
ALTER TABLE Team ADD CONSTRAINT FK_Team FOREIGN KEY (Captain) REFERENCES Geek (UserId);
ALTER TABLE Participation ADD CONSTRAINT FK_Participation1 FOREIGN KEY (Team) REFERENCES Team (TeamId);
ALTER TABLE Participation ADD CONSTRAINT FK_Participation2 FOREIGN KEY (Tournament) REFERENCES Tournament (TournamentId);
ALTER TABLE Versus ADD CONSTRAINT FK_Versus1 FOREIGN KEY (Team1) REFERENCES Team (TeamId);
ALTER TABLE Versus ADD CONSTRAINT FK_Versus2 FOREIGN KEY (Team2) REFERENCES Team (TeamId);
ALTER TABLE Versus ADD CONSTRAINT FK_Versus3 FOREIGN KEY (Tournament) REFERENCES Tournament (TournamentId);
ALTER TABLE Versus ADD CONSTRAINT FK_Versus4 FOREIGN KEY (VersusPeriod) REFERENCES Period (PeriodId);
ALTER TABLE Statistics ADD CONSTRAINT FK_Statistics1 FOREIGN KEY (Game) REFERENCES Game (GameId);
ALTER TABLE Statistics ADD CONSTRAINT FK_Statistics2 FOREIGN KEY (Team) REFERENCES Team (TeamId);
ALTER TABLE Statistics ADD CONSTRAINT FK_Statistics3 FOREIGN KEY (Player) REFERENCES Geek (UserId);
ALTER TABLE Round ADD CONSTRAINT FK_Round1 FOREIGN KEY (Versus) REFERENCES Versus (VersusId);
ALTER TABLE Round ADD CONSTRAINT FK_Round2 FOREIGN KEY (Result) REFERENCES Result (ResultId);
ALTER TABLE Score ADD CONSTRAINT FK_Score FOREIGN KEY (ScoreType) REFERENCES ScoreType (ScoreTypeId);
ALTER TABLE Result ADD CONSTRAINT FK_Result1 FOREIGN KEY (Loser) REFERENCES Team (TeamId);
ALTER TABLE Result ADD CONSTRAINT FK_Result2 FOREIGN KEY (Winner) REFERENCES Team (TeamId);
ALTER TABLE Configuration ADD CONSTRAINT FK_Configuration1 FOREIGN KEY (Geek) REFERENCES Geek (UserId);
ALTER TABLE Configuration ADD CONSTRAINT FK_Configuration2 FOREIGN KEY (Parameter) REFERENCES Parameter (ParameterId);
ALTER TABLE Roles_Geek ADD CONSTRAINT FK_RG1 FOREIGN KEY (Role) REFERENCES Roles (RoleId);
ALTER TABLE Roles_Geek ADD CONSTRAINT FK_RG2 FOREIGN KEY (Geek) REFERENCES Geek (UserId);
ALTER TABLE Team_Geek ADD CONSTRAINT FK_TG1 FOREIGN KEY (Team) REFERENCES Team (TeamId);
ALTER TABLE Team_Geek ADD CONSTRAINT FK_TG2 FOREIGN KEY (Player) REFERENCES Geek (UserId);
ALTER TABLE FollowGame ADD CONSTRAINT FK_FG1 FOREIGN KEY (Game) REFERENCES Game (GameId);
ALTER TABLE FollowGame ADD CONSTRAINT FK_FG2 FOREIGN KEY (Geek) REFERENCES Geek (UserId);
ALTER TABLE FollowPlayer ADD CONSTRAINT FK_FP1 FOREIGN KEY (Player) REFERENCES Geek (UserId);
ALTER TABLE FollowPlayer ADD CONSTRAINT FK_FP2 FOREIGN KEY (Geek) REFERENCES Geek (UserId);
ALTER TABLE FollowTeam ADD CONSTRAINT FK_FTe1 FOREIGN KEY (Team) REFERENCES Team (TeamId);
ALTER TABLE FollowTeam ADD CONSTRAINT FK_FTe2 FOREIGN KEY (Geek) REFERENCES Geek (UserId);
ALTER TABLE FollowTournament ADD CONSTRAINT FK_FTo1 FOREIGN KEY (Tournament) REFERENCES Tournament (TournamentId);
ALTER TABLE FollowTournament ADD CONSTRAINT FK_FTo2 FOREIGN KEY (Geek) REFERENCES Geek (UserId);
ALTER TABLE TournamentLog ADD CONSTRAINT FK_TL FOREIGN KEY (Tournament) REFERENCES Tournament (TournamentId);
ALTER TABLE TournamentTag ADD CONSTRAINT FK_ToT FOREIGN KEY (Tournament) REFERENCES Tournament (TournamentId);
ALTER TABLE TeamTag ADD CONSTRAINT FK_TeT FOREIGN KEY (Team) REFERENCES Team (TeamId);

INSERT INTO GameType VALUES ('FPS', 'First-Person Shooter', 'Pan-pan cul-cul', 0);
INSERT INTO GameType VALUES ('MOBA', 'Multiplayer Online Battle Arena', 'La map ressemble à un tangram pour enfant de 3 ans', 0);
INSERT INTO GameType VALUES ('MMORPG', 'Massively Multiplayer Online Role-Playing Game', 'Qui a pensé que ce serait une bonne idée pour une appli pareille ?', 1);

INSERT INTO Game VALUES (DEFAULT, 'Counter-Strike', 12, 'FPS', 0);
INSERT INTO Game VALUES (DEFAULT, 'Dota 2', 0, 'MOBA', 0);
INSERT INTO Game VALUES (DEFAULT, 'League of Legends', 0, 'MOBA', 1);

INSERT INTO Division VALUES (DEFAULT, 'Herald', 2, 0);
INSERT INTO Division VALUES (DEFAULT, 'Guardian', 2, 0);
INSERT INTO Division VALUES (DEFAULT, 'Crusader', 2, 0);
INSERT INTO Division VALUES (DEFAULT, 'Archon', 2, 0);
INSERT INTO Division VALUES (DEFAULT, 'Legend', 2, 0);
INSERT INTO Division VALUES (DEFAULT, 'Ancient', 2, 0);
INSERT INTO Division VALUES (DEFAULT, 'Divine', 2, 0);
INSERT INTO Division VALUES (DEFAULT, 'Immortal', 2, 0);
INSERT INTO Division VALUES (DEFAULT, 'Pro', 1, 0);
INSERT INTO Division VALUES (DEFAULT, 'Casual', 1, 0);

INSERT INTO ScoreType VALUES ('WIN', 'Victoire', 0);
INSERT INTO ScoreType VALUES ('CS', 'Nombre de creeps tués', 0);
INSERT INTO ScoreType VALUES ('KDA', '(Kills + Assists)/Deaths', 0);
INSERT INTO ScoreType VALUES ('OBJ', 'Nombre d''objectifs détruits', 1);

INSERT INTO Roles VALUES ('Player', 0);
INSERT INTO Roles VALUES ('Captain', 0);
INSERT INTO Roles VALUES ('Admin', 0);
INSERT INTO Roles VALUES ('Mod', 0);
INSERT INTO Roles VALUES ('Organiser', 0);
INSERT INTO Roles VALUES ('Weeaboo', 1);

INSERT INTO Reason VALUES ('Cheating', 'Déroger aux règles du jeu ou du tournoi', 0);
INSERT INTO Reason VALUES ('Betting', 'Parier sur un tournoi auquel l''utilisateur prend part', 1);
INSERT INTO Reason VALUES ('Swearing', 'Employer un langage un peu trop fleuri', 0);

INSERT INTO Geek VALUES (DEFAULT, 'Catwaman', 'Chaudanson', 'Jeanne', 'meaow666', 'catsrule@bastet.com', '20101995', 0);
INSERT INTO Geek VALUES (DEFAULT, 'Blyatman', 'Dosgor', 'Igor', 'p1zd1et', 'moskaumoskau@vodka.com', '26111995', 0);
INSERT INTO Geek VALUES (DEFAULT, 'Admin', 'Admin', 'Admin', 'admin', 'admin@admin.com', '05051905', 0);
INSERT INTO Geek VALUES (DEFAULT, 'ElModerator', 'Michel', 'Jean-Marie', 'iloveunicorns', 'pfudonr@gogole.com', '01021993', 0);
INSERT INTO Geek VALUES (DEFAULT, 'Deleted', 'Deleted', 'Deleted', 'deleted', 'deleted@deleted.com', '20022002', 1);
INSERT INTO Geek VALUES (DEFAULT, 'Dendi', 'Ishutin', 'Danil', 'squirrel321', 'danil@ishutin.com', '30121992', 0);

INSERT INTO Roles_Geek VALUES ('Admin', 3, 0);
INSERT INTO Roles_Geek VALUES ('Mod', 4, 0);
INSERT INTO Roles_Geek VALUES ('Organiser', 4, 0);
INSERT INTO Roles_Geek VALUES ('Captain', 1, 0);
INSERT INTO Roles_Geek VALUES ('Player', 1, 0);
INSERT INTO Roles_Geek VALUES ('Player', 6, 0);

INSERT INTO Message VALUES (DEFAULT, 'ez', 1509493171, 6, 1, 0);
INSERT INTO Message VALUES (DEFAULT, 'New phone who dis?', 1514203932, 2, 1, 1);
INSERT INTO Message VALUES (DEFAULT, 'Dernier avertissement !', 1539474055, 4, 2, 0);

INSERT INTO Period VALUES (DEFAULT, 1539514800, 1544785200, 0);
INSERT INTO Period VALUES (DEFAULT, 1538380800, 1556697600, 0);
INSERT INTO Period VALUES (DEFAULT, 1565028000, 1566774000, 0);

INSERT INTO Ban VALUES (DEFAULT, 'Avertissement ignoré', 2, 1, 'Swearing', 0);

INSERT INTO Team VALUES ('PWRRGR', 'Power Rangers', '', 1, 0);

INSERT INTO Team_Geek VALUES (1, 'PWRRGR', 0);
INSERT INTO Team_Geek VALUES (6, 'PWRRGR', 0);
INSERT INTO Team_Geek VALUES (2, 'PWRRGR', 1);

INSERT INTO Ranking VALUES (1, 2, 1, 'PWRRGR', 0);

INSERT INTO Tournament VALUES ('TI42', 'The International 42', 'Que le meilleur gagne !', 16, 2, '', 2, 3, 2, 0, 4);

INSERT INTO Participation VALUES (NULL, 1, 'PWRRGR', 'TI42', 0);

INSERT INTO Parameter VALUES ('Langue', 'en', 0);
INSERT INTO Parameter VALUES ('Messages', 'on', 0);

INSERT INTO Configuration VALUES (1, 'Langue', 'fr', 0);
INSERT INTO Configuration VALUES (1, 'Messages', 'on', 0);
INSERT INTO Configuration VALUES (2, 'Langue', 'en', 0);
INSERT INTO Configuration VALUES (2, 'Messages', 'on', 0);
INSERT INTO Configuration VALUES (6, 'Langue', 'en', 0);
INSERT INTO Configuration VALUES (6, 'Messages', 'off', 0);
INSERT INTO Configuration VALUES (3, 'Langue', 'fr', 0);
INSERT INTO Configuration VALUES (3, 'Messages', 'off', 0);
INSERT INTO Configuration VALUES (4, 'Langue', 'fr', 0);
INSERT INTO Configuration VALUES (4, 'Messages', 'on', 0);
INSERT INTO Configuration VALUES (5, 'Langue', 'fr', 1);
INSERT INTO Configuration VALUES (5, 'Messages', 'on', 1);

INSERT INTO FollowTeam VALUES (4, 'PWRRGR', 0);

INSERT INTO FollowPlayer VALUES (1, 6, 0);

INSERT INTO FollowGame VALUES (2, 1, 1);
INSERT INTO FollowGame VALUES (2, 2, 0);

INSERT INTO FollowTournament VALUES (4, 'TI42', 0);

INSERT INTO TournamentTag VALUES(DEFAULT, 'TI42', "Shangai", 0);
INSERT INTO TournamentTag VALUES(DEFAULT, 'TI42', "China", 0);
INSERT INTO TournamentTag VALUES(DEFAULT, 'TI42', "icefrog", 0);
INSERT INTO TournamentTag VALUES(DEFAULT, 'TI42', "Tupac", 1);

INSERT INTO TeamTag VALUES(DEFAULT, 'PWRRGR', "force", 0);
INSERT INTO TeamTag VALUES(DEFAULT, 'PWRRGR', "bleue", 0);
INSERT INTO TeamTag VALUES(DEFAULT, 'PWRRGR', "jaune", 0);
INSERT INTO TeamTag VALUES(DEFAULT, 'PWRRGR', "rouge", 0);
INSERT INTO TeamTag VALUES(DEFAULT, 'PWRRGR', "multicolore", 1);