/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.Participation;
import com.thearena.hibernate.ParticipationId;
import java.util.List;

/**
 *
 * @author Jeanba
 */
public interface ParticipationDAO {
    public void createParticipation (Participation participation) throws DAOException;
    public void createOrUpdateParticipation (Participation participation) throws DAOException;
    public void updateParticipation (Participation participation) throws DAOException;
    public boolean deleteParticipation (ParticipationId participationId) throws DAOException;
    public List<Participation> getParticipations() throws DAOException;
    public Participation getParticipationById(ParticipationId idParticipation) throws DAOException;    
}
