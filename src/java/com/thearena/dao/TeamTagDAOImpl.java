/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.HibernateUtil;
import com.thearena.hibernate.Team;
import com.thearena.hibernate.Teamtag;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author nhuch
 */
public class TeamTagDAOImpl implements TeamTagDAO {
    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;
	
    public void setSessionFactory(SessionFactory sessionFactory){
            this.sessionFactory = sessionFactory;
    }

    public TeamTagDAOImpl() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }
    @Override
    public List<Teamtag> selectTeamTag() throws DAOException {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            List<Teamtag> list = session.createQuery("from Teamtag where Deleted = false").list();
            transaction.commit();
            return list;
        } catch (HibernateException hibernateException) {
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite pendant la récupération des tags.");
        } finally {
            session.close();
        }
    }

    @Override
    public Teamtag selectTeamTagById(int id) throws DAOException {
        try {
           session = this.sessionFactory.openSession();
            Transaction tx = session.beginTransaction();       
            Teamtag t = (Teamtag) session.get(Teamtag.class, id);
            tx.commit();
            session.close();
            return t; 
        } catch (HibernateException ex) {
            throw new DAOException("Une erreur s'est produite lors de la récupération des tags.");
        }
    }

    @Override
    public void insertTag(Teamtag tag) throws DAOException {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            session.persist(tag);
            transaction.commit();
        } catch (HibernateException hibernateException) {
            transaction.rollback();
            hibernateException.getStackTrace();
            throw new DAOException("Une erreur s'est produite pendant l'ajout d'un tag.");
        } finally {
            session.close();
        }
    }
    @Override
    public void insertOrUpdateTag(Teamtag tag) throws DAOException {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            session.saveOrUpdate(tag);
            transaction.commit();
        } catch (HibernateException hibernateException) {
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite pendant l'ajout d'un tag.");
        } finally {
            session.close();
        }
    }

    @Override
    public void deleteTag(int id) throws DAOException {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("update Teamtag set Deleted = true where TagId = :entryId");
            query.setParameter("entryId", id);
            int i = query.executeUpdate();
            transaction.commit();
        } catch (HibernateException hibernateException) {
            hibernateException.printStackTrace();
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite pendant la suppression du tag.");
        } finally {
            session.close();
        }
    }
    
    @Override
    public Teamtag getTeamTagByTeamNameAndTag(String tag,Team team) {
        session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();       
        Criteria criteria = session.createCriteria(Teamtag.class);
        Teamtag tt = (Teamtag) criteria.add(Restrictions.eq("team", team)).add(Restrictions.eq("tag",tag))
                             .uniqueResult();
        tx.commit();
        return tt;
    }
}
