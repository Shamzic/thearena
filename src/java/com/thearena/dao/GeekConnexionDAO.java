/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.Geek;
import java.util.List;

/**
 *
 * @author shame
 */
public interface GeekConnexionDAO {
    public void addGeek(Geek p);
    public void updateGeek(Geek p);
    public List<Geek> listGeeks();
    public Geek getGeekById(int id);
    public void removeGeek(int id);
}
