/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.Game;
import com.thearena.hibernate.HibernateUtil;
import com.thearena.hibernate.Tournament;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Jeanba
 */

public class TournamentDAOImpl implements TournamentDAO{
    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;
	
    public void setSessionFactory(SessionFactory sf){
            this.sessionFactory = sf;
    }
    
    public TournamentDAOImpl() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }
        
    @Override
    public void addTournament(Tournament p) throws DAOException {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            session.persist(p.getPeriodByPlayingPeriod());
            session.persist(p.getPeriodByRegisteringPeriod());
            session.persist(p);
            transaction.commit();
        } catch (HibernateException hibernateException) {
            hibernateException.printStackTrace();            
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite pendant l'ajout du tournoi.");
        } finally {
            session.close();
        }
    }

    @Override
    public void updateTournament(Tournament p) throws DAOException {
        try {
            session = this.sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.update(p.getPeriodByPlayingPeriod());
            session.update(p.getPeriodByRegisteringPeriod());
            session.update(p);
            transaction.commit();
        } catch (HibernateException hibernateException) {
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite pendant la mise à jour du tournoi.");
        } finally {
            session.close();
        }
    }

    @Override
    public List<Tournament> listTournament() throws DAOException {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            List<Tournament> list = session.createQuery("from Tournament where Deleted = false").list();
            transaction.commit();
            return list;
        } catch (HibernateException hibernateException) {
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite pendant la récupération des tournois.");
        } 
    }

    @Override
    public Tournament getTournamentById(String id) throws DAOException {
        try {
            session = this.sessionFactory.openSession();
            transaction = session.beginTransaction();            
            Tournament t = (Tournament) session.get(Tournament.class, id);
            transaction.commit();
            return t;
        } catch (HibernateException hibernateException) {
            transaction.rollback();
            session.close();
            throw new DAOException("Une erreur s'est produite pendant la récupération du tournoi.");
        }
    }

    @Override
    public boolean removeTournament(String id) throws DAOException {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();

            Query query = session.createQuery("update Tournament set Deleted = true where tournamentId = :tournamentId");
            query.setParameter("tournamentId", id);
            int i = query.executeUpdate();
            
            transaction.commit();
            return (i > 0);
        } catch (HibernateException hibernateException) {
            hibernateException.printStackTrace();
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite pendant la suppression du tournoi.");
        } 
    }
    
    @Override
    public List<Game> getGamesPlayed() throws DAOException{
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            List<Game> list = session.createQuery("select DISTINCT(game) from Tournament t where t.deleted = false").list();
            transaction.commit();
            return list;
        } catch (HibernateException hibernateException) {
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite pendant la récupération des jeux joués.");
        } 
    }
    
}
