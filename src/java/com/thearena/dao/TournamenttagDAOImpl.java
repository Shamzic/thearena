/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.HibernateUtil;
import com.thearena.hibernate.Tournament;
import com.thearena.hibernate.Tournamenttag;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;


/**
 *
 * @author Jeanba
 */
public class TournamenttagDAOImpl implements TournamenttagDAO{
    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;
	
    public void setSessionFactory(SessionFactory sessionFactory){
            this.sessionFactory = sessionFactory;
    }

    public TournamenttagDAOImpl() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }
    @Override
    public List<Tournamenttag> selectTournamentTag() throws DAOException {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            List<Tournamenttag> list = session.createQuery("from Tournamenttag where Deleted = false").list();
            transaction.commit();
            return list;
        } catch (HibernateException hibernateException) {
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite pendant la récupération des tags.");
        } finally {
            session.close();
        }
    }

    @Override
    public Tournamenttag selectTournamentTagById(int id) throws DAOException {
        try {
           session = this.sessionFactory.openSession();
            Transaction tx = session.beginTransaction();       
            Tournamenttag t = (Tournamenttag) session.get(Tournamenttag.class, id);
            tx.commit();
            session.close();
            return t; 
        } catch (HibernateException ex) {
            throw new DAOException("Une erreur s'est produite lors de la récupération des tags.");
        }
    }
    @Override
    public Tournamenttag selectTournamentTagByTournamentAndTag(Tournament tournament, String tag) throws DAOException {
        try {
           session = this.sessionFactory.openSession();
            Transaction tx = session.beginTransaction();       
            Criteria criteria = session.createCriteria(Tournamenttag.class);
            Tournamenttag tt = (Tournamenttag) criteria.add(Restrictions.eq("tournament", tournament)).add(Restrictions.eq("tag",tag))
                             .uniqueResult();
            tx.commit();
            return tt; 
        } catch (HibernateException ex) {
            throw new DAOException("Une erreur s'est produite lors de la récupération des tags.");
        } finally {
            session.close();
        }
    }

    @Override
    public void insertTag(Tournamenttag tag) throws DAOException {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            session.persist(tag);
            transaction.commit();
        } catch (HibernateException hibernateException) {
            transaction.rollback();
            hibernateException.getStackTrace();
            throw new DAOException("Une erreur s'est produite pendant l'ajout d'un tag.");
        } finally {
            session.close();
        }
    }
    @Override
    public void insertOrUpdateTag(Tournamenttag tag) throws DAOException {
        
        try {
            System.out.println(tag.getTournament().getTournamentId());
            Tournamenttag tournamentTag = selectTournamentTagByTournamentAndTag(tag.getTournament(), tag.getTag());
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            if (tournamentTag != null) {
                tournamentTag.setDeleted(Boolean.FALSE);
                tag = tournamentTag;
            }                
            
            session.saveOrUpdate(tag);
            transaction.commit();
        } catch (HibernateException hibernateException) {
            hibernateException.printStackTrace();
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite pendant l'ajout d'un tag.");
        } finally {

        }
    }

    @Override
    public void deleteTag(int id) throws DAOException {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();

            Query query = session.createQuery("update Tournamenttag set Deleted = true where tagId = :tagId");
            query.setParameter("tagId", id);
            int i = query.executeUpdate();
            
            transaction.commit();
        } catch (HibernateException hibernateException) {
            hibernateException.printStackTrace();
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite pendant la suppression du tag.");
        } finally {
            session.close();
        }
    }
    
}
