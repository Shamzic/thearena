/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.Geek;
import com.thearena.hibernate.HibernateUtil;
import com.thearena.hibernate.Team;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author nhuch
 */
public class InscriptionDAOImpl implements InscriptionDAO {
    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;
    private String alreadyRegisteredMessage;
    private String passwordMessage;
	
    public void setSessionFactory(SessionFactory sf){
            this.sessionFactory = sf;
    }
    
    public InscriptionDAOImpl() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }
        
    @Override
    public void addGeek(Geek g) {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            session.persist(g);
            transaction.commit();
        } catch (HibernateException hibernateException) {
            hibernateException.printStackTrace();
            transaction.rollback();
        }
    }

    @Override
    public void updateGeek(Geek g) {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            session.update(g);
            transaction.commit();
	} 
        catch (HibernateException hibernateException) {
            hibernateException.printStackTrace();
            transaction.rollback();
        } 
   }

    @Override
    public List<Geek> listGeek() {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            List<Geek> list = session.createQuery("from Geek").list();
            transaction.commit();
            return list;
        } catch (HibernateException hibernateException) {
            transaction.rollback();
        }
        return null;
    }

    @Override
    public Geek getGeekByUsername(String username) {
        session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();       
        Criteria criteria = session.createCriteria(Geek.class);
        Geek g = (Geek) criteria.add(Restrictions.eq("username", username.toLowerCase()))
                             .uniqueResult();
        tx.commit();
        return g;
    }
    
        public Geek getGeekByIdBis(String username) {
        session = this.sessionFactory.getCurrentSession();
        Transaction tx = session.beginTransaction();       
        Geek g = (Geek) session.get(Geek.class, username);
        tx.commit();
        return g;
    }

    @Override
    public void removeGeek(String username) {
        session = this.sessionFactory.getCurrentSession();
    }
    
    @Override
    public boolean canGeekBeAdded(Geek g, String usernameEntered, String pass1, String pass2) {
        if(this.testUsername(g, usernameEntered) && testPassword( pass1, pass2))
            return true; 
        else 
            return false;
    }
    
    @Override
    public boolean testUsername(Geek g, String usernameEntered) {
        session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();       
        Geek geekTest = getGeekByUsername(usernameEntered);
        tx.commit();
        if(geekTest == null) {
            this.setAlreadyRegisteredMessage(null);
            return true;
        }
        else {
            this.setAlreadyRegisteredMessage("Ce nom d'utilisateur est déjà pris! Veuillez recommencer");
            return false;
        }
    }
    
    @Override
    public boolean testPassword(String password1, String password2) {   
        if((password1.equals(password2)==true) && password1.length()>=3 ) {
            System.out.println("les deux mots de passes sont :"+ password1+", et :" + password2);
            this.setPasswordMessage(null);
            return true;
        }
        else {
            this.setPasswordMessage("Les mots de passe doivent être identiques et avoir une taille supérieure à 3 caractères");
            return false;
        }
    }
    
    
    @Override    
    public void setAlreadyRegisteredMessage(String message) {
        this.alreadyRegisteredMessage = message;
    }
    
    @Override
    public String getAlreadyRegisteredMessage() {
        return this.alreadyRegisteredMessage;
    }
    
    @Override
    public void setPasswordMessage(String message) {
        this.passwordMessage = message;
    }
    
    @Override
    public String getPasswordMessage() {
        return this.passwordMessage;
    }
}
