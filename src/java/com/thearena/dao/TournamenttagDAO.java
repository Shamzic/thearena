/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.Tournament;
import com.thearena.hibernate.Tournamenttag;
import java.util.List;

/**
 *
 * @author Jeanba
 */
public interface TournamenttagDAO {
    public List<Tournamenttag> selectTournamentTag() throws DAOException;
    public Tournamenttag selectTournamentTagById(int id) throws DAOException;
    public Tournamenttag selectTournamentTagByTournamentAndTag(Tournament tournament, String tag) throws DAOException;
    public void insertTag(Tournamenttag tag) throws DAOException;
    public void insertOrUpdateTag(Tournamenttag tag) throws DAOException;
    public void deleteTag(int id) throws DAOException;
}
