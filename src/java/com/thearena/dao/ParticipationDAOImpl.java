/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.HibernateUtil;
import com.thearena.hibernate.Participation;
import com.thearena.hibernate.ParticipationId;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Jeanba
 */
public class ParticipationDAOImpl implements ParticipationDAO {
    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;
    
    public void setSessionFactory(SessionFactory sf){
            this.sessionFactory = sf;
    }

    public ParticipationDAOImpl() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }
    

    @Override
    public void createParticipation(Participation participation)  throws DAOException {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            session.persist(participation);
            transaction.commit();
        } catch (HibernateException hibernateException) {
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite pendant l'enregistrement de la participation.");
        } finally {
            session.close();
        }
    }
    
    @Override
    public void createOrUpdateParticipation(Participation participation)  throws DAOException {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            session.saveOrUpdate(participation);
            transaction.commit();
        } catch (HibernateException hibernateException) {
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite pendant la création ou mise à jour de la participation.");
        } finally {
            session.close();
        }
    }

    @Override
    public void updateParticipation(Participation participation)  throws DAOException {
        try {
            session = this.sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            session.update(participation);
            tx.commit();
            session.close();
        } catch (HibernateException hibernateException) {
            throw new DAOException("Une erreur s'est produite pendant la mise à jour de la participation.");
        }
    }

    @Override
    public boolean deleteParticipation(ParticipationId participationId)  throws DAOException {
        try {
            Participation participation = getParticipationById(participationId);
            transaction = session.beginTransaction();
            participation.setDeleted(Boolean.TRUE);            
            transaction.commit();
            return (true);
        } catch (HibernateException hibernateException) {
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite pendant la suppression de la participation.");
        } 
    }

    @Override
    public List<Participation> getParticipations() throws DAOException {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            List<Participation> list = session.createQuery("from Participation where Deleted = false").list();
            transaction.commit();
            return list;
        } catch (HibernateException hibernateException) {
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite pendant la récupération des participations.");
        } 
    }

    @Override
    public Participation getParticipationById(ParticipationId idParticipation) throws DAOException {
        try {
            session = this.sessionFactory.openSession();
            Transaction tx = session.beginTransaction();            
            Participation t = (Participation) session.get(Participation.class, idParticipation);
            tx.commit();
            return t;
        } catch (HibernateException hibernateException) {
            throw new DAOException("Une erreur s'est produite pendant la récupération de la participation.");
        }
    }
    
}
