/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.Tournamentlog;
import java.util.List;

/**
 *
 * @author Jeanba
 */
public interface TournamentlogDAO {
    public List<Tournamentlog> selectTournamentlog() throws DAOException;
    public Tournamentlog selectTournamentlogById(int id) throws DAOException;
    public void insertLog(Tournamentlog log) throws DAOException;
    public void deleteLog(int id) throws DAOException;
}
