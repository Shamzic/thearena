/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.Geek;
import com.thearena.hibernate.HibernateUtil;
import com.thearena.hibernate.Team;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 *
 * @author nhuch
 */
public class ConnexionDAOImpl implements ConnexionDAO {
    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;
    private String resultat;
	
    public void setSessionFactory(SessionFactory sf){
            this.sessionFactory = sf;
    }
    
    public ConnexionDAOImpl() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
        this.resultat = "";
    }
    
        
    @Override
    public void addGeek(Geek g) {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            session.persist(g);
            transaction.commit();
        } catch (HibernateException hibernateException) {
            hibernateException.printStackTrace();
            transaction.rollback();
        }
    }

    @Override
    public void updateGeek(Geek g) {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            session.update(g);
            transaction.commit();
	} 
        catch (HibernateException hibernateException) {
            hibernateException.printStackTrace();
            transaction.rollback();
        } 
   }

    @Override
    public List<Geek> listGeek() {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            List<Geek> list = session.createQuery("from Geek").list();
            transaction.commit();
            return list;
        } catch (HibernateException hibernateException) {
            transaction.rollback();
        }
        return null;
    }

    @Override
    public Geek getGeekByUsername(String username) {
        session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();       
        Criteria criteria = session.createCriteria(Geek.class);
        Geek g = (Geek) criteria.add(Restrictions.eq("username", username.toLowerCase()))
                             .uniqueResult();
        tx.commit();
        return g;
    }
    
        public Geek getGeekByIdBis(String username) {
        session = this.sessionFactory.getCurrentSession();
        Transaction tx = session.beginTransaction();       
        Geek g = (Geek) session.get(Geek.class, username);
        tx.commit();
        return g;
    }

    @Override
    public void removeGeek(String username) {
        session = this.sessionFactory.getCurrentSession();
    }

    @Override
    public Boolean isPasswordCorresponding(String username, String passwordToTest) {
        session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();       
        Geek g = getGeekByUsername(username);
        tx.commit();
        if(g == null) {
            setResultat("L'utilisateur n'existe pas! Veuillez recommencer");
            return false;
        }
        else {
            System.out.println("password entré : "+passwordToTest);
            System.out.println("password bd : "+g.getPassword());
            if (g.getPassword().equals(passwordToTest)) {
                setResultat("Bienvenue "+ username + " !");
                System.out.println("Les pass coincident : "+g.getPassword());
                return true;
            }
            else {
                setResultat("Erreur d'entrée sur le mot de passe");
                return false;
            }
        }
    }

    @Override
    public String getResultat() {
        return this.resultat; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setResultat(String resultat) {
       this.resultat = resultat;
    }
}
