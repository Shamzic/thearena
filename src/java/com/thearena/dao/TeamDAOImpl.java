/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.HibernateUtil;
import com.thearena.hibernate.Team;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author nhuch
 */
public class TeamDAOImpl implements TeamDAO {
    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;
	
    public void setSessionFactory(SessionFactory sf){
            this.sessionFactory = sf;
    }
    
    public TeamDAOImpl() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }
        
    @Override
    public void addTeam(Team p) throws DAOException{
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            session.persist(p);
            transaction.commit();
        } catch (HibernateException hibernateException) {
            hibernateException.printStackTrace();            
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite pendant l'ajout de l'équipe.");
        }
    }

    @Override
    public void updateTeam(Team p) throws DAOException{
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            session.update(p);
            transaction.commit();
	} 
        catch (HibernateException hibernateException) {
            hibernateException.printStackTrace();            
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite pendant l'update de l'équipe.");
        } 
   }

    @Override
    public List<Team> listTeam() throws DAOException{
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            List<Team> list = session.createQuery("from Team where deleted=false").list();
            transaction.commit();
            return list;
        } catch (HibernateException hibernateException) {
            hibernateException.printStackTrace();            
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite pendant la récupération des équipes.");
        } 
    }

    @Override
    public Team getTeamById(String id) throws DAOException{
        session = this.sessionFactory.openSession();
        try{
        Transaction tx = session.beginTransaction();       
        Team t = (Team) session.get(Team.class, id);
        tx.commit();
        return t;
        }
        catch (HibernateException hibernateException) {
            hibernateException.printStackTrace();            
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite pendant la récupération de l'équipe.");
        } 
    }
    
    @Override
        public Team getTeamByIdBis(String id) throws DAOException{
        session = this.sessionFactory.getCurrentSession();
        try {
        Transaction tx = session.beginTransaction();   
        Team t = (Team) session.get(Team.class, id);
        tx.commit();
        return t;
        }catch (HibernateException hibernateException) {
            hibernateException.printStackTrace();            
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite pendant la récupération de l'équipe.");
        }
    }

    @Override
    public boolean removeTeam(String id) throws DAOException{
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();

            Query query = session.createQuery("update Team set Deleted = true where teamId = :teamId");
            query.setParameter("teamId", id);
            int i = query.executeUpdate();
            
            transaction.commit();
            return (i > 0);
        } catch (HibernateException hibernateException) {
            hibernateException.printStackTrace();
            transaction.rollback();
            return false;
        } 
    }
    
    
    @Override
    public Team getTeamByName(String name) throws DAOException{
        session = this.sessionFactory.openSession();
        try{
            Transaction tx = session.beginTransaction();       
            Criteria criteria = session.createCriteria(Team.class);
            Team t = (Team) criteria.add(Restrictions.eq("name", name))
                                 .uniqueResult();
            tx.commit();
            return t;
        } catch (HibernateException hibernateException) {
            hibernateException.printStackTrace();            
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite pendant la récupération de l'équipe.");
        } 
    }
}
