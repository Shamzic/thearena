/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.Team;
import com.thearena.hibernate.Teamtag;
import java.util.List;

/**
 *
 * @author nhuch
 */
public interface TeamTagDAO {    
    public List<Teamtag> selectTeamTag() throws DAOException;
    public Teamtag selectTeamTagById(int id) throws DAOException;
    public void insertTag(Teamtag tag) throws DAOException;
    public void insertOrUpdateTag(Teamtag tag) throws DAOException;
    public void deleteTag(int id) throws DAOException;
    public Teamtag getTeamTagByTeamNameAndTag(String tag,Team team);
}
