/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author ASUS PC
 */

public class SearchDAOImpl implements SearchDAO{
    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;
	
    public void setSessionFactory(SessionFactory sf){
            this.sessionFactory = sf;
    }
    
    public SearchDAOImpl() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }

    @Override
    public List<String> getTournamentsByTag(String tag) {
        session = this.sessionFactory.openSession();
        
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("SELECT tournament.tournamentId FROM Tournamenttag WHERE tag = :tag AND deleted = false");
            query.setParameter("tag", tag);
            List results = query.list();
            transaction.commit();
            return results;
        } catch (HibernateException hibernateException) {
            transaction.rollback();
        } 
           
        return null;
    }
    
    @Override
    public List<String> getTeamsByTag(String tag) {
        session = this.sessionFactory.openSession();

        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("SELECT team.teamId FROM Teamtag WHERE tag = :tag AND deleted = false");
            query.setParameter("tag", tag);
            List results = query.list();
            transaction.commit();
            return results;
        } catch (HibernateException hibernateException) {
            transaction.rollback();
        } 
        
        return null;
    }
    
    @Override
    public List<String> teamSearch(int captain, int player){
        List listC = this.captainSearch(captain);
        List listP = this.playerSearch(player);
        
        if (listP != null && listC != null){
            // select only geeks in both searches
            listP.retainAll(listC);
            return listP;
        } else {
            return null;
        }

    }
    
    @Override
    public List<String> captainSearch(int captain){
        session = this.sessionFactory.openSession();

        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("SELECT teamId FROM Team WHERE geek.userId = :captain AND deleted = false");
            query.setParameter("captain", captain);
            List results = query.list();
            transaction.commit();
            return results;
        } catch (HibernateException hibernateException) {
            transaction.rollback();
        } 
        
        return null;
    }
    
    @Override
    public List<String> playerSearch(int player){
                session = this.sessionFactory.openSession();

        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("SELECT team.teamId FROM TeamGeek WHERE geek.userId = :player AND deleted = false");
            query.setParameter("player", player);
            List results = query.list();
            transaction.commit();
            return results;
        } catch (HibernateException hibernateException) {
            transaction.rollback();
        } 
        
        return null;
    }
    
     @Override
    public List<String> tournamentSearch(int organiser, int game){
        List listO = this.organiserSearch(organiser);
        //System.out.println(listO);
        List listG = this.gameSearch(game);
        //System.out.println(listG);
        
        if (listO != null && listG != null){
            // select only geeks in both searches
            listO.retainAll(listG);
            return listO;
        } else {
            return null;
        }

    }
    
    @Override
    public List<String> organiserSearch(int organiser){
        session = this.sessionFactory.openSession();

        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("SELECT tournamentId FROM Tournament WHERE geek.userId = :organiser AND deleted = false");
            query.setParameter("organiser", organiser);
            List results = query.list();
            transaction.commit();
            return results;
        } catch (HibernateException hibernateException) {
            transaction.rollback();
        } 
        
        return null;
    }
    
    @Override
    public List<String> gameSearch(int game){
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("SELECT tournamentId FROM Tournament WHERE game.gameId = :game AND deleted = false");
            query.setParameter("game", game);
            List results = query.list();
            transaction.commit();
            return results;
        } catch (HibernateException hibernateException) {
            transaction.rollback();
        } 
        
        return null;
    }
}
