/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.Geek;
import com.thearena.hibernate.Team;
import java.util.List;

/**
 *
 * @author nhuch
 */
public interface InscriptionDAO {
    public void addGeek(Geek g);
    public void updateGeek(Geek g);
    public List<Geek> listGeek();
    public Geek getGeekByUsername(String username);
    public void removeGeek(String username);
    public boolean canGeekBeAdded(Geek g, String usernameEntered, String pass1, String pass2);
    public boolean testUsername(Geek g, String usernameEntered);
    public boolean testPassword(String password1, String password2);
    public void setAlreadyRegisteredMessage(String message);
    public String getAlreadyRegisteredMessage();
    public void setPasswordMessage(String message);
    public String getPasswordMessage();
    
    
}
