/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.HibernateUtil;
import com.thearena.hibernate.Tournamentlog;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Jeanba
 */
public class TournamentlogDAOImpl implements TournamentlogDAO{
    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;
	
    public void setSessionFactory(SessionFactory sf){
            this.sessionFactory = sf;
    }
    
    public TournamentlogDAOImpl() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }
    @Override
    public List<Tournamentlog> selectTournamentlog() throws DAOException{
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            List<Tournamentlog> list = session.createQuery("from Tournamentlog where Deleted = false").list();
            transaction.commit();
            return list;
        } catch (HibernateException hibernateException) {
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite pendant la récupération des messages de log.");
        } finally {
            session.close();
        }
    }

    @Override
    public Tournamentlog selectTournamentlogById(int id) throws DAOException{
        try {
           session = this.sessionFactory.openSession();
            Transaction tx = session.beginTransaction();       
            Tournamentlog t = (Tournamentlog) session.get(Tournamentlog.class, id);
            tx.commit();
            session.close();
            return t; 
        } catch (HibernateException ex) {
            throw new DAOException("Une erreur s'est produite lors de la récupération du message de log.");
        }
        
    }

    

    @Override
    public void insertLog(Tournamentlog log) throws DAOException{
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            session.persist(log);
            transaction.commit();
        } catch (HibernateException hibernateException) {
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite pendant l'ajout du message de log.");
        } finally {
            session.close();
        }
    }

    @Override
    public void deleteLog(int id) throws DAOException{
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();

            Query query = session.createQuery("update Tournamentlog set Deleted = true where entryId = :entryId");
            query.setParameter("entryId", id);
            int i = query.executeUpdate();
            
            transaction.commit();
            session.close();
        } catch (HibernateException hibernateException) {
            hibernateException.printStackTrace();
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite pendant la suppression du log.");
        } finally {
            session.close();
        }
    }
    
}
