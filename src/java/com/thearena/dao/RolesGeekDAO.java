/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.Geek;
import com.thearena.hibernate.RolesGeek;
import com.thearena.hibernate.RolesGeekId;
import java.util.List;

/**
 *
 * @author Jeanba
 */
public interface RolesGeekDAO {
    public List<RolesGeek> selectRolesGeek() throws DAOException;
    public List<RolesGeek> selectRolesGeekForGeek(Geek geek) throws DAOException;
    public RolesGeek getRolesGeekById(RolesGeekId rolesGeekId) throws DAOException;
    public void deleteRolesGeek(RolesGeekId rolesGeekId) throws DAOException;
    public void addRolesGeek(RolesGeek rolesGeek) throws DAOException;
    public List<Geek> getGeeksByRole(String role);
}
