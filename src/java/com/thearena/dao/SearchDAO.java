/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import java.util.List;

/**
 *
 * @author ASUS PC
 */
 
public interface SearchDAO {
    public List<String> getTournamentsByTag(String tag);
    public List<String> getTeamsByTag(String tag);
    public List<String> teamSearch(int captain, int player);
    public List<String> captainSearch(int captain);
    public List<String> playerSearch(int player);
    public List<String> tournamentSearch(int organiser, int game);
    public List<String> organiserSearch(int organiser);
    public List<String> gameSearch(int game);
}
