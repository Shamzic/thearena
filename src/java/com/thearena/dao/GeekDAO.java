package com.thearena.dao;

import com.thearena.hibernate.Geek;
import java.util.List;

public interface GeekDAO {
    
    public List<Geek> listGeek();
    public Geek getGeekByUsername(String s);
    public void updateGeek(Geek g);
    
}
