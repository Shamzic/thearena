/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.TeamGeek;
import com.thearena.hibernate.TeamGeekId;
import java.util.List;

/**
 *
 * @author nhuch
 */
public interface TeamGeekDAO {
    public void addTeamGeek(TeamGeek p);
    public void updateTeamGeek(TeamGeek p);
    public List<TeamGeek> listTeamGeek();
    public TeamGeek getTeamGeekById(TeamGeekId id);
    public void removeTeamGeek(TeamGeek p);
    public void adOrUpdateTeamGeek(TeamGeek p);
}