/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.Geek;
import com.thearena.hibernate.HibernateUtil;
import com.thearena.hibernate.RolesGeek;
import com.thearena.hibernate.RolesGeekId;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Jeanba
 */
public class RolesGeekDAOImpl implements RolesGeekDAO{
    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;
    
    public void setSessionFactory(SessionFactory sf){
            this.sessionFactory = sf;
    }

    public RolesGeekDAOImpl() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }
    
    @Override
    public List<RolesGeek> selectRolesGeek() throws DAOException {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            List<RolesGeek> list = session.createQuery("from RolesGeek where Deleted = false").list();
            transaction.commit();
            return list;
        } catch (HibernateException hibernateException) {
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite lors de la récupération des Roles.");
        }
    }

    @Override
    public List<RolesGeek> selectRolesGeekForGeek(Geek geek) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteRolesGeek(RolesGeekId rolesGeekId) throws DAOException {
        try {
            RolesGeek participation = getRolesGeekById(rolesGeekId);
            transaction = session.beginTransaction();
            participation.setDeleted(Boolean.TRUE);
            
            
            //int i = query.executeUpdate();
            
            transaction.commit();
        } catch (HibernateException hibernateException) {
            hibernateException.printStackTrace();
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite lors de la suppression du rôle.");
        } 
    }

    @Override
    public void addRolesGeek(RolesGeek rolesGeek) throws DAOException {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            session.saveOrUpdate(rolesGeek);
            transaction.commit();
        } catch (HibernateException hibernateException) {
            hibernateException.printStackTrace();
            transaction.rollback();
            throw new DAOException("Une erreur s'est produite lors de l'ajout du rôle.");
        } 
    }

    @Override
    public RolesGeek getRolesGeekById(RolesGeekId rolesGeekId) throws DAOException {
        try {
            session = this.sessionFactory.openSession();
            Transaction tx = session.beginTransaction();            
            RolesGeek t = (RolesGeek) session.get(RolesGeek.class, rolesGeekId);
            tx.commit();
            return t;
        } catch (HibernateException hibernateException) {
            throw new DAOException("Une erreur s'est produite lors de la récupération du role.");
        }
    }
    
    @Override 
    public List<Geek> getGeeksByRole(String role){
        session = this.sessionFactory.openSession();
        
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("SELECT geek FROM RolesGeek rg WHERE rg.deleted = false AND rg.roles.roleId = :role");
            query.setParameter("role", role);
            List results = query.list();
            transaction.commit();
            return results;
        } catch (HibernateException hibernateException) {
            transaction.rollback();
        } 
           
        return null;
    }
    
}
