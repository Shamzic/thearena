/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.Game;
import java.util.List;

/**
 *
 * @author Jeanba
 */
public interface GameDAO {
    public List<Game> listGame();
}
