/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.HibernateUtil;
import com.thearena.hibernate.TeamGeek;
import com.thearena.hibernate.TeamGeekId;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author nhuch
 */
public class TeamGeekDAOImpl implements TeamGeekDAO {
    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;
	
    public void setSessionFactory(SessionFactory sf){
            this.sessionFactory = sf;
    }
    
    public TeamGeekDAOImpl() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }
        
    @Override
    public void addTeamGeek(TeamGeek p) {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            session.persist(p);
            transaction.commit();
        } catch (HibernateException hibernateException) {
            hibernateException.printStackTrace();
            transaction.rollback();
        } 
    }

    @Override
    public void updateTeamGeek(TeamGeek p) {
         session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            session.update(p);
            transaction.commit();
	} 
        catch (HibernateException hibernateException) {
            hibernateException.printStackTrace();
            transaction.rollback();
        } 
   }

    @Override
    public List<TeamGeek> listTeamGeek() {
        return null;
    }

    @Override
    public TeamGeek getTeamGeekById(TeamGeekId id) {
        session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();       
        TeamGeek t = (TeamGeek) session.get(TeamGeek.class, id);
        tx.commit();
        return t;
    }


    @Override
    public void removeTeamGeek(TeamGeek p) {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            session.update(p);
            transaction.commit();
	} 
        catch (HibernateException hibernateException) {
            hibernateException.printStackTrace();
            transaction.rollback();
        } 
    }
    
        @Override
        public void adOrUpdateTeamGeek(TeamGeek p) {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            session.saveOrUpdate(p);
            transaction.commit();
	} 
        catch (HibernateException hibernateException) {
            hibernateException.printStackTrace();
            transaction.rollback();
        } 
    }
}
