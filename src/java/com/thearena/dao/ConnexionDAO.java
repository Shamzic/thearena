/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.Geek;
import com.thearena.hibernate.Team;
import java.util.List;

/**
 *
 * @author nhuch
 */
public interface ConnexionDAO {
    public void addGeek(Geek g);
    public void updateGeek(Geek g);
    public List<Geek> listGeek();
    public Geek getGeekByUsername(String username);
    public Boolean isPasswordCorresponding(String username, String password);
    public void removeGeek(String username);
    public String getResultat();
    public void setResultat(String resultat);
}
