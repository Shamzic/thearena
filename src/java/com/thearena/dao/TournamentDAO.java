/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.Game;
import com.thearena.hibernate.Tournament;
import java.util.List;

/**
 *
 * @author Jeanba
 */
public interface TournamentDAO {
    public void addTournament(Tournament p) throws DAOException;
    public void updateTournament(Tournament p) throws DAOException;
    public List<Tournament> listTournament() throws DAOException;
    public Tournament getTournamentById(String id) throws DAOException;
    public boolean removeTournament(String id) throws DAOException;
    public List<Game> getGamesPlayed()throws DAOException;
}
