/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.Geek;
import com.thearena.hibernate.HibernateUtil;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author shame
 */

public class GeekConnexionDAOImpl implements GeekConnexionDAO{
    private SessionFactory sessionFactory;
	
    public void setSessionFactory(SessionFactory sf){
            this.sessionFactory = sf;
    }
    
    public GeekConnexionDAOImpl() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }
        
    @Override
    public void addGeek(Geek p) {
        Session session = this.sessionFactory.getCurrentSession();
        Transaction tx = null;
        try {
           tx = session.beginTransaction();
           session.persist(p);
           tx.commit();
        }
        catch (RuntimeException e) {
           tx.rollback();
           throw e;
        }
    }

    @Override
    public void updateGeek(Geek p) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @return
     */
    @Override
    public List<Geek> listGeeks() {
        Session session = this.sessionFactory.getCurrentSession();
        Transaction t = session.beginTransaction();
        List<Geek> list = session.createQuery("from Geek").list();
        t.commit();
        return list;
    }

    @Override
    public Geek getGeekById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Geek  t = (Geek) session.load(Geek.class, session);
        return t;
    }

    @Override
    public void removeGeek(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
