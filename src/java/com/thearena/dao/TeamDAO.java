/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.Team;
import java.util.List;

/**
 *
 * @author nhuch
 */
public interface TeamDAO {
    public void addTeam(Team p)throws DAOException;
    public void updateTeam(Team p)throws DAOException;
    public List<Team> listTeam()throws DAOException;
    public Team getTeamById(String id)throws DAOException;
    public Team getTeamByIdBis(String id)throws DAOException;
    public boolean removeTeam(String id)throws DAOException;
    public Team getTeamByName(String name)throws DAOException;
}
