/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.dao;

import com.thearena.hibernate.Game;
import com.thearena.hibernate.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Jeanba
 */
public class GameDAOImpl implements GameDAO {
    SessionFactory sessionFactory;
    Session session;
    Transaction transaction;
    
    public GameDAOImpl() {
        sessionFactory = HibernateUtil.getSessionFactory();
    }
    @Override
    public List<Game> listGame() {
        session = this.sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            List<Game> list = session.createQuery("from Game").list();
            transaction.commit();
            session.close();
            return list;
        } catch (HibernateException hibernateException) {
            transaction.rollback();
        } 
        return null;
    }
    
}
