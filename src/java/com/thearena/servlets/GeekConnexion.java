/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.servlets;

import com.thearena.dao.TournamentDAOImpl;
import com.thearena.dao.GeekConnexionDAOImpl;
import com.thearena.hibernate.Geek;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.thearena.dao.GeekConnexionDAO;

/**
 *
 * @author shame
 */
public class GeekConnexion extends HttpServlet {

        public GeekConnexion() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        GeekConnexionDAOImpl geekDAO = new GeekConnexionDAOImpl();
        List<Geek> geeks = geekDAO.listGeeks();
        request.setAttribute("geeks", geeks);
        this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/index.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            doGet(request, response);
    }
    
    
    @Override
    public String getServletInfo() {
        return "Servlet permettant la connexion d'un";
    }// </editor-fold>

}
