/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.service;

import com.thearena.hibernate.Tournament;

/**
 *
 * @author Jeanba
 */
public interface TournamenttagService {

    void addOrUpdateTag(Tournament tournoi, String tag) throws ServiceException;

    void addTag(Tournament tournoi, String tag) throws ServiceException;

    void addTags(Tournament tournoi, String tags) throws ServiceException;

    void deleteTag(int tagId) throws ServiceException;
    
}
