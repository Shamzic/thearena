/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.service;

import com.thearena.hibernate.Participation;
import com.thearena.hibernate.Team;
import com.thearena.hibernate.Tournament;

/**
 *
 * @author Jeanba
 */
public interface ParticipationService {

    void addParticipation(Tournament tournament, Team team) throws ServiceException;

    boolean checkAuthorisationToCreate(String teamId) throws ServiceException;

    // Mélange des méthodes d'update et de création : Le chef d'équipe, les admins et le créateur du tournoi peuvent modifier une participation.
    boolean checkAuthorisationToDelete(String teamId, String tournamentId) throws ServiceException;

    void deleteParticipation(String tournamentId, Team team) throws ServiceException;

    void updateParticipation(Participation participation, String tournamentId) throws ServiceException;
    
}
