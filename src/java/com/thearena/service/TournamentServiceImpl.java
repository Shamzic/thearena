/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.service;

import com.thearena.beans.PeriodDate;
import com.thearena.beans.UtilisateurDetails;
import com.thearena.dao.DAOException;
import com.thearena.dao.TournamentDAO;
import com.thearena.dao.TournamentDAOImpl;
import com.thearena.hibernate.Geek;
import com.thearena.hibernate.Roles;
import com.thearena.hibernate.Tournament;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 *
 * @author Jeanba
 */
@Service
public class TournamentServiceImpl implements TournamentService {
    TournamentlogService tournamentlogService = new TournamentlogServiceImpl();
    TournamenttagServiceImpl tagService = new TournamenttagServiceImpl();
    TournamentDAO tournamentDAO = new TournamentDAOImpl();
    /** GETTER */
    /**
     * 
     * @return
     * @throws ServiceException 
     */
    @Override
    public List<Tournament> selectTournaments() throws ServiceException {
        try {
            return tournamentDAO.listTournament();
        } catch (DAOException ex) {
            throw new ServiceException (ex.getMessage());
        }
    }
    @Override
    public Tournament getTournamentById(String tournamentId) throws ServiceException {
        try {
            Tournament tournoi = tournamentDAO.getTournamentById(tournamentId);
            if (tournoi == null || tournoi.getDeleted())
                throw new ServiceException("Ce tournoi n'existe pas/plus.");
            return tournoi;
        } catch (DAOException dAOException) {
            throw new ServiceException(dAOException.getMessage());
        }
    }
    /** CREATE READ UPDATE */
    /**
     * Création de tournoi.
     * @param tournament Objet tournoi
     * @param registeringPeriod Période d'inscription
     * @param playingPeriod Période de jeu
     * @throws ServiceException Erreur lors de la création
     */
    @Override
    public void createTournament(Tournament tournament, PeriodDate registeringPeriod, PeriodDate playingPeriod ) throws ServiceException {       
        try {
            /**
             * Gestion des périodes de jeu
             */
            
            tournament.setPeriodByPlayingPeriod(playingPeriod.getPeriod());
            tournament.setPeriodByRegisteringPeriod(registeringPeriod.getPeriod());
            
            tournament.setDeleted(Boolean.FALSE);
            tournament.setGeek(getAuthenticatedGeek());
            
            if (tournament.getTournamentId() == null || tournament.getTournamentId().isEmpty()) {
                UUID uuid = UUID.randomUUID();
                tournament.setTournamentId(uuid.toString());
            }
            
            tournament.setDeleted(Boolean.FALSE);
            /** Vérifications des valeurs/des doublons */
            checkTournamentValues(tournament);
            checkDoublon(tournament, false);
            /**
             * Vérification du tournoi données et doublons
             */
            tournamentDAO.addTournament(tournament);
            /**
             * Ajout des tags rentré par l'utilisateur et du nom
             */
            tagService.addTags(tournament, tournament.getTags());
            tagService.addTag(tournament, tournament.getName());
            /**
             * Ajout du rôle "Organiser" au créateur.
             */
            RolesGeekServiceImpl rolesGeekService = new RolesGeekServiceImpl();
            rolesGeekService.addRolesGeek(getAuthenticatedGeek(), new Roles("Organiser"));
            /**
             * Ajout du message de log
             */
            tournamentlogService.addLog(tournament, "Le tournoi a été créé avec succès.");
        } catch (ServiceException serviceException) {
            throw serviceException;
        } catch (DAOException dAOException) {
            throw new ServiceException(dAOException.getMessage());
        } catch (Exception ex) {
            throw new ServiceException("Une erreur s'est produite pendant la génération de la requête.");
        }
    }
    /**
     * Mise à jour de tournoi.
     * @param tournament Objet tournoi
     * @param registeringPeriod Période d'inscription
     * @param playingPeriod Période de jeu
     * @throws ServiceException Erreur lors de la mise à jour
     */
    @Override
    public void updateTournament(Tournament tournament, PeriodDate registeringPeriod, PeriodDate playingPeriod ) throws ServiceException {
        try {
            checkAuthorisationToUpdate(tournament);
            
            tournamentlogService.addLog(tournament, "Tentative de modification du tournoi.");
            
            tournament.setDeleted(Boolean.FALSE);
            registeringPeriod.setTimePeriod(tournament.getPeriodByRegisteringPeriod());
            playingPeriod.setTimePeriod(tournament.getPeriodByPlayingPeriod());
            
            /** Vérification des valeurs */
            checkTournamentValues(tournament);
            checkDoublon(tournament, true);
            
            tournamentDAO.updateTournament(tournament);
            
            tagService.addOrUpdateTag(tournament, tournament.getName());
            tournamentlogService.addLog(tournament, "Le tournoi a ete modifie.");
        } catch (DAOException dAOException) {
            throw new ServiceException(dAOException.getMessage());
        }
    }
    @Override
    public void deleteTournament(String tournamentId) throws ServiceException{
        try {
            if (!checkAuthorisationToUpdate(tournamentId))
                throw new ServiceException("Vous n'avez pas l'autorisation d'effectuer cette action.");
            tournamentDAO.removeTournament(tournamentId);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
    }    
    
    @Override
    public Geek getAuthenticatedGeek() throws ServiceException {
       Authentication auth = SecurityContextHolder.getContext().getAuthentication();      
        UtilisateurDetails user = (UtilisateurDetails)auth.getPrincipal();
        return user.getDatabaseObject();
    }
    /**
     * Cette méthode pour vérifier l'accès de l'utilisateur à partir du créateur d'un tournoi.
     * @param tournoi Objet du tournois
     * @return
     * @throws ServiceException 
     */
    @Override
    public boolean checkAuthorisationToUpdate(Tournament tournoi) throws ServiceException {
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if (auth.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN")))
                return true;
            return (Objects.equals(getAuthenticatedGeek().getUserId(), tournoi.getGeek().getUserId()));
        } catch (ServiceException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new ServiceException("Une erreur est survenue pendant l'authentification.");
        }
    }
    /**
     * Cette méthode pour vérifier l'accès de l'utilisateur part de l'objet Geek stocké en BD. 
     * @param tournamentId ID du tournois
     * @return
     * @throws ServiceException 
     */
    @Override
    public boolean checkAuthorisationToUpdate(String tournamentId) throws ServiceException {
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if (auth.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN")))
                return true;
            Geek authenticatedGeek = getAuthenticatedGeek();
            return authenticatedGeek.getTournaments().stream()
                .anyMatch(t -> Objects.equals(
                        ((Tournament)t).getGeek().getUserId(), authenticatedGeek.getUserId()
                ));
        } catch (ServiceException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new ServiceException("Une erreur est survenue pendant l'authentification.");
        }
    }
    /**
     * Permet de vérifier si l'ID du tournoi n'est pas déjà existante et si le nom du tournoi n'existe pas déjà
     * @param tournoi 
     * @param update Doit être placé à true si il s'agit uniquement d'une update.
     * @throws ServiceException Si l'id/le nom est déjà pris, on renvoie une erreur.
     */
    @Override
    public void checkDoublon(Tournament tournoi, boolean update) throws ServiceException {
        if(!update && selectTournaments().stream()
                .anyMatch(t -> t.getName().equals(tournoi.getName()) 
                        ||  t.getTournamentId().equals(tournoi.getTournamentId()))
                )
            throw new ServiceException("Le nom/id du tournoi est déjà utilisé.");
        if(update && selectTournaments().stream()
                .anyMatch(t -> t.getName().equals(tournoi.getName()) 
                        &&  !t.getTournamentId().equals(tournoi.getTournamentId()))
                )
            throw new ServiceException("Le nom de tournoi est déjà utilisé.");
    }
    /**
     * Cette méthode va vérifier que l'ensemble des valeurs insérées soient correctes.
     * @param tournoi
     * @throws ServiceException 
     */
    @Override
    public void checkTournamentValues(Tournament tournoi) throws ServiceException {
        try {
            // Vérifier que l'ID ainsi que le nom ne sont pas vides
            if(tournoi.getTournamentId().isEmpty() || tournoi.getName().isEmpty())
                throw new ServiceException("Merci de rentrer un nom non vide.");
            // Vérifier que la date de début de jeux soit avant la date de fin
            if(tournoi.getPeriodByPlayingPeriod().getStart() > tournoi.getPeriodByPlayingPeriod().getEnd())
                throw new ServiceException("Merci de rentrer une date de fin de jeu supérieure à celle de début.");
            // Vérifier que la date de début de l'inscription soit avant la date de fin
            if(tournoi.getPeriodByRegisteringPeriod().getStart() > tournoi.getPeriodByRegisteringPeriod().getEnd())
                throw new ServiceException("Merci de rentrer une date de fin d'inscription supérieure à celle de début.");
            // Vérifier que la date de début du tournoi soit après la date de fin d'inscription.
            if(tournoi.getPeriodByRegisteringPeriod().getEnd() > tournoi.getPeriodByPlayingPeriod().getStart())
                throw new ServiceException("Merci de rentrer une date de jeu supérieure aux dates d'inscription.");
            // Vérifier que le début de l'inscription soit après la date d'aujourd'hui
            if(tournoi.getPeriodByRegisteringPeriod().getStart() < startOfDayTimestamp())
                throw new ServiceException("Merci de rentrer une date d'inscription supérieure à la date actuelle.");
            // Vérifier le nombre de joueurs et le nombre d'équipes soit non null et supérieur à 0
            if(tournoi.getPlayerNumber() == null || tournoi.getPlayerNumber() <= 0)
                throw new ServiceException("Merci de rentrer un nombre de joueurs par équipe supérieur 0.");
            if(tournoi.getSlots() == null || tournoi.getSlots() <= 0)
                throw new ServiceException("Merci de rentrer un nombre de joueurs par équipe supérieur 0.");
        } catch (ServiceException e) {
            throw e;
        } 
    }
    public int startOfDayTimestamp() {
        LocalDateTime localDateTime = LocalDateTime.now();
        LocalDateTime startOfDay = localDateTime.with(LocalTime.MIN);
        return (int)(startOfDay.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()/1000);
    }
}
