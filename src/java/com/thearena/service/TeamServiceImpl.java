/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.service;

import com.thearena.beans.UtilisateurDetails;
import com.thearena.dao.DAOException;
import com.thearena.dao.TeamDAO;
import com.thearena.dao.TeamDAOImpl;
import com.thearena.dao.TeamGeekDAO;
import com.thearena.dao.TeamGeekDAOImpl;
import com.thearena.hibernate.Geek;
import com.thearena.hibernate.Roles;
import com.thearena.hibernate.Team;
import com.thearena.hibernate.TeamGeek;
import java.util.List;
import java.util.Objects;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import java.util.UUID;

/**
 *
 * @author nhuch
 */
public class TeamServiceImpl implements TeamService{
    TeamDAO teamDAO = new TeamDAOImpl();
    
    @Override
    public void addTeam(Team team, TeamGeek teamGeek) throws ServiceException{
         try {
            Team testTeam = teamDAO.getTeamByName(team.getName());
            if(team.getName().isEmpty()){
                throw new ServiceException("Le nom d'équipe ne doit pas être vide");
            }
            if(testTeam != null){
                throw new ServiceException("Ce nom d'équipe est déjà utilisé");
            }
            else{
                teamDAO.addTeam(team);
                TeamGeekDAO teamGeekDAO = new TeamGeekDAOImpl();
                teamGeekDAO.addTeamGeek(teamGeek);
                TeamtagService teamtagService = new TeamtagServiceImpl();
                teamtagService.addTags(team, team.getTags());
                RolesGeekServiceImpl rolesGeekService = new RolesGeekServiceImpl();
                rolesGeekService.addRolesGeek(getAuthenticatedGeek(), new Roles("Captain"));
            }
        }catch (Exception ex) {
            ex.printStackTrace();
            throw new ServiceException(ex.getMessage());
        }
    }
    
    @Override
    public boolean verificationTeamId(Team team) throws ServiceException{
        try {
        if(teamDAO.getTeamById(team.getTeamId()) == null){
            return true;
        }
        else{
            UUID uuid = UUID.randomUUID();
            team.setTeamId(uuid.toString());
            return verificationTeamId(team);
        }
        }catch (Exception ex) {
            ex.printStackTrace();
            throw new ServiceException("Une erreur s'est produite lors de la vérification de l'id.");
        }
    }
    
    @Override
    public void updateTeam(Team team) throws ServiceException{
        try {
        Team testTeam = teamDAO.getTeamByName(team.getName());
        if(testTeam != null){
           throw new ServiceException("Ce nom d'équipe est déjà pris");
        }
        else if(team.getName().isEmpty()){
                throw new ServiceException("Le nom d'équipe ne doit pas être vide");
            }
        else{
            teamDAO.updateTeam(team);
        }
        }catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
    }
    
    public Geek getAuthenticatedGeek() throws ServiceException {
       Authentication auth = SecurityContextHolder.getContext().getAuthentication();      
        UtilisateurDetails user = (UtilisateurDetails)auth.getPrincipal();
        return user.getDatabaseObject();
    }
    
    @Override
    public List<Team> listTeam()throws ServiceException{
        try {
            return teamDAO.listTeam();
        } catch (DAOException ex) {
            throw new ServiceException (ex.getMessage());
        }
    }
    
    @Override
    public Team getTeamById(String teamId) throws ServiceException {
        try {
            Team team = teamDAO.getTeamById(teamId);
            if ((team) == null || team.getDeleted())
                throw new ServiceException("Cette équipe n'existe pas/plus.");
            return team;
        } catch (DAOException dAOException) {
            throw new ServiceException(dAOException.getMessage());
        }
    }
    
    @Override
    public Team getTeamByIdBis(String tournamentId) throws ServiceException {
        try {
            Team team = teamDAO.getTeamByIdBis(tournamentId);
            if ((team) == null || team.getDeleted())
                throw new ServiceException("Cette équipe n'existe pas/plus.");
            return team;
        } catch (DAOException dAOException) {
            throw new ServiceException(dAOException.getMessage());
        }
    }
    @Override
    public void deleteTeam(String teamId) throws ServiceException{
        try {
            if (!checkAuthorisationToUpdate(teamId))
                throw new ServiceException("Vous n'avez pas l'autorisation d'effectuer cette action.");
            teamDAO.removeTeam(teamId);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
    } 
    
    public boolean checkAuthorisationToUpdate(String team) throws ServiceException {
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if (auth.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN")))
                return true;
            Geek authenticatedGeek = getAuthenticatedGeek();
            return authenticatedGeek.getTournaments().stream()
                .anyMatch(t -> Objects.equals(
                        ((Team)t).getGeek().getUserId(), authenticatedGeek.getUserId()
                ));
        } catch (ServiceException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new ServiceException("Une erreur est survenue pendant l'authentification.");
        }
    }
}
