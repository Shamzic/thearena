/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.service;

import com.thearena.hibernate.Tournament;

/**
 *
 * @author Jeanba
 */
public interface TournamentlogService {

    void addLog(Tournament tournament, String message) throws ServiceException;
    
}
