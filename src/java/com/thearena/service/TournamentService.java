/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.service;

import com.thearena.beans.PeriodDate;
import com.thearena.hibernate.Geek;
import com.thearena.hibernate.Tournament;
import java.util.List;

/**
 *
 * @author Jeanba
 */
public interface TournamentService {

    /**
     * Cette méthode pour vérifier l'accès de l'utilisateur à partir du créateur d'un tournoi.
     * @param tournoi Objet du tournois
     * @return
     * @throws ServiceException
     */
    boolean checkAuthorisationToUpdate(Tournament tournoi) throws ServiceException;

    /**
     * Cette méthode pour vérifier l'accès de l'utilisateur part de l'objet Geek stocké en BD.
     * @param tournamentId ID du tournois
     * @return
     * @throws ServiceException
     */
    boolean checkAuthorisationToUpdate(String tournamentId) throws ServiceException;

    /**
     * Permet de vérifier si l'ID du tournoi n'est pas déjà existante et si le nom du tournoi n'existe pas déjà
     * @param tournoi
     * @param update Doit être placé à true si il s'agit uniquement d'une update.
     * @throws ServiceException Si l'id/le nom est déjà pris, on renvoie une erreur.
     */
    void checkDoublon(Tournament tournoi, boolean update) throws ServiceException;

    /**
     * Cette méthode va vérifier que l'ensemble des valeurs insérées soient correctes.
     * @param tournoi
     * @throws ServiceException
     */
    void checkTournamentValues(Tournament tournoi) throws ServiceException;

    /** CREATE READ UPDATE */
    /**
     * Création de tournoi.
     * @param tournament Objet tournoi
     * @param registeringPeriod Période d'inscription
     * @param playingPeriod Période de jeu
     * @throws ServiceException Erreur lors de la création
     */
    void createTournament(Tournament tournament, PeriodDate registeringPeriod, PeriodDate playingPeriod) throws ServiceException;

    void deleteTournament(String tournamentId) throws ServiceException;

    Geek getAuthenticatedGeek() throws ServiceException;

    Tournament getTournamentById(String tournamentId) throws ServiceException;

    /** GETTER */
    /**
     *
     * @return
     * @throws ServiceException
     */
    List<Tournament> selectTournaments() throws ServiceException;

    /**
     * Mise à jour de tournoi.
     * @param tournament Objet tournoi
     * @param registeringPeriod Période d'inscription
     * @param playingPeriod Période de jeu
     * @throws ServiceException Erreur lors de la mise à jour
     */
    void updateTournament(Tournament tournament, PeriodDate registeringPeriod, PeriodDate playingPeriod) throws ServiceException;
    
}
