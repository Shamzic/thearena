/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.service;

import com.thearena.dao.DAOException;
import com.thearena.dao.ParticipationDAO;
import com.thearena.dao.ParticipationDAOImpl;
import com.thearena.hibernate.Geek;
import com.thearena.hibernate.Participation;
import com.thearena.hibernate.ParticipationId;
import com.thearena.hibernate.Team;
import com.thearena.hibernate.Tournament;

/**
 *
 * @author Jeanba
 */
public class ParticipationServiceImpl implements ParticipationService {
    ParticipationDAO participationDAO = new ParticipationDAOImpl();
    TournamentlogService tournamentlogService = new TournamentlogServiceImpl();
    TournamentService tournamentService = new TournamentServiceImpl();
    
    @Override
    public void addParticipation(Tournament tournament, Team team) throws ServiceException {
        try {
            if (!checkAuthorisationToCreate(team.getTeamId())) {
                throw new ServiceException("Cette action n'est pas autorisée.");
            }
            Participation participation = new Participation(new ParticipationId(team.getTeamId(), tournament.getTournamentId()), team, tournament);
            participation.setDeleted(Boolean.FALSE);
            participation.setQualified(Boolean.FALSE);
            participation.setPlace(0);
            participationDAO.createOrUpdateParticipation(participation);
            tournamentlogService.addLog(tournament, "L'equipe " + team.getTeamId() + " a ete rajoutee.");
        } catch (ServiceException serviceException) {
            throw serviceException;
        } catch (DAOException dAOException) {
            throw new ServiceException(dAOException.getMessage());
        }
    }
    
    @Override
    public void updateParticipation(Participation participation, String tournamentId) throws ServiceException {
        try {
            if(!tournamentService.checkAuthorisationToUpdate(tournamentId))
                throw new ServiceException("Vous n'avez pas l'autorisation d'effectuer cette action.");
            Tournament tournament = new Tournament();
            tournament.setTournamentId(tournamentId);

            participation.setId(new ParticipationId(participation.getTeam().getTeamId(), tournamentId));
            participationDAO.updateParticipation(participation);
            tournamentlogService.addLog(tournament, "La participation de l'Ã©quipe " + participation.getTeam().getTeamId() + " a Ã©tÃ© mise Ã  jour.");
        } catch (DAOException ex ) {
            throw new ServiceException(ex.getMessage());
        }
    }
    
    @Override
    public void deleteParticipation(String tournamentId, Team team) throws ServiceException {
        try {
            if (!checkAuthorisationToDelete(team.getTeamId(), tournamentId)) {
                throw new ServiceException("Vous n'avez pas l'autorisation d'effectuer cette action.");
            }
            Tournament tournament = new Tournament();
            tournament.setTournamentId(tournamentId);
            participationDAO.deleteParticipation(new ParticipationId(team.getTeamId(), tournamentId));            
            tournamentlogService.addLog(tournament, "La participation de l'equipe " + team.getTeamId() + " a ete supprimee.");
        } catch (ServiceException serviceException) {
            throw serviceException;
        } catch (DAOException dAOException) {
            throw new ServiceException(dAOException.getMessage());
        }
    }
    
    @Override
    public boolean checkAuthorisationToCreate(String teamId) throws ServiceException {
        // Seul le Captain d'une équipe peut créer une participation, donc on vérfie qu'il est bien capitaine de l'équipe qu'il essaye de rajouter.
        Geek authGeek = tournamentService.getAuthenticatedGeek();
        return authGeek.getTeams().stream().anyMatch(t -> ((Team)t).getGeek().getUserId().equals(authGeek.getUserId()));
    }
    // Mélange des méthodes d'update et de création : Le chef d'équipe, les admins et le créateur du tournoi peuvent modifier une participation.
    @Override
    public boolean checkAuthorisationToDelete(String teamId, String tournamentId) throws ServiceException {
        return (checkAuthorisationToCreate(teamId) || tournamentService.checkAuthorisationToUpdate(tournamentId));
    }

}
