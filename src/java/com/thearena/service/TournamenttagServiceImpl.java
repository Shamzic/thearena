/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.service;

import com.thearena.dao.DAOException;
import com.thearena.dao.TournamenttagDAO;
import com.thearena.dao.TournamenttagDAOImpl;
import com.thearena.hibernate.Tournament;
import com.thearena.hibernate.Tournamenttag;

/**
 *
 * @author Jeanba
 */
public class TournamenttagServiceImpl implements TournamenttagService {
    TournamenttagDAO tournamenttagDAO = new TournamenttagDAOImpl();
    
    @Override
    public void addTags(Tournament tournoi, String tags) throws ServiceException {
        try {
        String[] tagsArray = tags.split(",");
        for(String tag: tagsArray) {
            addTag(tournoi, tag);
        }
        } catch  (ServiceException daoEx) {
            throw new ServiceException(daoEx.getMessage());
        }
    }
    
    @Override
    public void addTag(Tournament tournoi, String tag) throws ServiceException {
        
        try {
            if (!tag.isEmpty()) {
                Tournamenttag tagObj = new Tournamenttag(); 

                tagObj.setTournament(tournoi);

                tagObj.setTag(tag.trim());

                tagObj.setDeleted(false);

                tournamenttagDAO.insertOrUpdateTag(tagObj);

            }
        } catch (DAOException ex){
            throw new ServiceException(ex.getMessage());
        }
        catch (Exception ex) {
            ex.printStackTrace();
            throw new ServiceException("Une erreur s'est produite lors de la creation d'un tag.");
        }
    }
    
    @Override
    public void addOrUpdateTag(Tournament tournoi, String tag) throws ServiceException {
        
        try {
            if (!tag.isEmpty()) {
                Tournamenttag tagObj = new Tournamenttag(); 

                tagObj.setTournament(tournoi);

                tagObj.setTag(tag.trim());

                tagObj.setDeleted(false);

                tournamenttagDAO.insertOrUpdateTag(tagObj);

            }
        } catch (DAOException ex){
            throw new ServiceException(ex.getMessage());
        }
        catch (Exception ex) {
            ex.printStackTrace();
            throw new ServiceException("Une erreur s'est produite lors de la creation d'un tag.");
        }
    }
    
    @Override
    public void deleteTag(int tagId) throws ServiceException {
        try {
            tournamenttagDAO.deleteTag(tagId);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
        catch (Exception ex) {
            throw new ServiceException("Une erreur s'est passé pendant la suppression d'un tag.");
        }        
    }
}
