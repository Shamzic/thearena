/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.service;

import com.thearena.beans.UtilisateurDetails;
import com.thearena.dao.DAOException;
import com.thearena.dao.RolesGeekDAO;
import com.thearena.dao.RolesGeekDAOImpl;
import com.thearena.hibernate.Geek;
import com.thearena.hibernate.Roles;
import com.thearena.hibernate.RolesGeek;
import com.thearena.hibernate.RolesGeekId;
import com.thearena.security.DynamicAuthenticationWrapper;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 *
 * @author Jeanba
 */
@Service
public class RolesGeekServiceImpl {
    RolesGeekDAO rolesGeekDAO = new RolesGeekDAOImpl();
    
    public void addRolesGeek(Geek geek, Roles role) throws ServiceException {
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();      
            UtilisateurDetails user = (UtilisateurDetails)auth.getPrincipal();
            RolesGeek roleToAdd = new RolesGeek();
            roleToAdd.setGeek(geek);
            roleToAdd.setRoles(role);
            roleToAdd.setDeleted(Boolean.FALSE);
            roleToAdd.setId(new RolesGeekId(role.getRoleId(), geek.getUserId()));
            rolesGeekDAO.addRolesGeek(roleToAdd);
            
            if (geek == user.getDatabaseObject()) {
                List<GrantedAuthority> newRoles = new ArrayList<>();
                newRoles.add(new SimpleGrantedAuthority("ROLE_" + role.getRoleId().toUpperCase()));
                DynamicAuthenticationWrapper authentificationWrapper = new DynamicAuthenticationWrapper(auth, newRoles);
                SecurityContextHolder.getContext().setAuthentication(authentificationWrapper);
            }
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        } catch (Exception ex) {
            throw new ServiceException("Une erreur est survenue lors de l'ajout d'un rôle.");
        }
    }
    
    public void deleteRolesGeek(RolesGeekId rolesGeekId) throws ServiceException {
        try {
            rolesGeekDAO.deleteRolesGeek(rolesGeekId);
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
    }
}
