/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.service;

import com.thearena.dao.DAOException;
import com.thearena.dao.TeamTagDAO;
import com.thearena.dao.TeamTagDAOImpl;
import com.thearena.hibernate.Team;
import com.thearena.hibernate.Teamtag;
import java.util.List;

/**
 *
 * @author nhuch
 */
public class TeamtagServiceImpl implements TeamtagService {
    TeamTagDAO teamtagDAO = new TeamTagDAOImpl();
    
    @Override
    public void addTags(Team team, String tags) throws ServiceException {
        try {
        String[] tagsArray = tags.split(",");
        for(String tag: tagsArray) {
            addTag(team, tag);
        }
        } catch  (ServiceException daoEx) {
            throw new ServiceException(daoEx.getMessage());
        }
    }
    
    @Override
    public void addTag(Team team, String tag) throws ServiceException {
        
        try {
            if (!tag.isEmpty()) {
                Teamtag tagObj = new Teamtag(); 
                tagObj.setTeam(team);
                tagObj.setTag(tag.trim());
                tagObj.setDeleted(false);
                teamtagDAO.insertOrUpdateTag(tagObj);

            }
        } catch (DAOException ex){
            throw new ServiceException(ex.getMessage());
        }
        catch (Exception ex) {
            ex.printStackTrace();
            throw new ServiceException("Une erreur s'est produite lors de la creation d'un tag.");
        }
    }
    
    @Override
    public void insertOrUpdateTag(Team team, String tag) throws ServiceException{
        try {
            if (!tag.isEmpty()) {
                Teamtag teamtag = teamtagDAO.getTeamTagByTeamNameAndTag(tag, team);
                if(teamtag != null){
                    teamtag.getTeam().getName();
                    teamtag.setDeleted(false);
                    teamtagDAO.insertOrUpdateTag(teamtag);
                }else{
                    addTag(team,tag);
                }
            }
        } catch (DAOException ex){
                throw new ServiceException(ex.getMessage());
        }
        catch (Exception ex) {
                ex.printStackTrace();
                throw new ServiceException("Une erreur s'est produite lors de la creation d'un tag.");
        }
    }
    
    
    @Override
    public void deleteTag(Teamtag tag) throws DAOException, ServiceException{
        try {
            teamtagDAO.deleteTag(tag.getTagId());
        } catch (DAOException ex) {
            throw new ServiceException(ex.getMessage());
        }
        catch (Exception ex) {
            throw new ServiceException("Une erreur s'est passé pendant la suppression d'un tag.");
        } 
    }
}
