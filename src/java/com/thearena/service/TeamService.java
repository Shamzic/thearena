/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.service;

import com.thearena.hibernate.Team;
import com.thearena.hibernate.TeamGeek;
import java.util.List;


/**
 *
 * @author nhuch
 */
public interface TeamService {
    public void addTeam(Team team, TeamGeek teamGeek) throws ServiceException;
    public List<Team> listTeam() throws ServiceException;
    public boolean verificationTeamId(Team team) throws ServiceException;
    public Team getTeamByIdBis(String tournamentId) throws ServiceException;
    public Team getTeamById(String tournamentId) throws ServiceException;
    public void updateTeam(Team team) throws ServiceException;
    public void deleteTeam(String teamId) throws ServiceException;
}
