/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.service;

import com.thearena.dao.ParticipationDAOImpl;
import com.thearena.dao.TeamGeekDAO;
import com.thearena.dao.TeamGeekDAOImpl;
import com.thearena.hibernate.Participation;
import com.thearena.hibernate.Team;
import com.thearena.hibernate.TeamGeek;
import java.util.List;

/**
 *
 * @author nhuch
 */
public class TeamGeekServiceImpl implements TeamGeekService{
    TeamGeekDAO teamGeekDAO = new TeamGeekDAOImpl();
    @Override
    public void addOrUpdateTeamGeek(TeamGeek teamGeek) throws ServiceException{
        try{
            Team team=teamGeek.getTeam();
            List<Participation> participations = new ParticipationDAOImpl().getParticipations();
            if(!participations.isEmpty()){
                boolean condition = true;
                for(int k=0;k<participations.size();k++){
                    if(participations.get(k).getTeam().getTeamId().equals(teamGeek.getTeam().getTeamId())){
                        System.out.println("test1");
                        if(!participations.get(k).getDeleted()){
                            System.out.println("test2");
                            if(!participations.get(k).getTournament().getDeleted()){
                                System.out.println("test3");
                                condition=false;
                            }
                        }
                    }
                }
                if(condition==true){
                    teamGeekDAO.adOrUpdateTeamGeek(teamGeek);
                }
            }
            else{
                teamGeekDAO.adOrUpdateTeamGeek(teamGeek);
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
            throw new ServiceException("Une erreur s'est produite lors de l'inscription a l'équipe.");
        }
    }
    
    @Override
    public void updateTeamGeek(TeamGeek teamGeek)throws ServiceException{
        try{
            Team team=teamGeek.getTeam();
            List<Participation> participations = new ParticipationDAOImpl().getParticipations();
            if(!participations.isEmpty()){
                boolean condition = true;
                for(int k=0;k<participations.size();k++){
                    if(participations.get(k).getTeam().getTeamId().equals(teamGeek.getTeam().getTeamId())){
                        System.out.println("test1");
                        if(!participations.get(k).getDeleted()){
                            System.out.println("test2");
                            if(!participations.get(k).getTournament().getDeleted()){
                                System.out.println("test3");
                                condition=false;
                            }
                        }
                    }
                }
                if(condition==true){
                    teamGeekDAO.updateTeamGeek(teamGeek);
                }
            }
            else{
                teamGeekDAO.updateTeamGeek(teamGeek);
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
            throw new ServiceException("Une erreur s'est produite lors de l'inscription a l'équipe.");
        }
    }
}
