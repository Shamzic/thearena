/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.service;

import com.thearena.dao.DAOException;
import com.thearena.hibernate.Team;
import com.thearena.hibernate.Teamtag;

/**
 *
 * @author nhuch
 */
public interface TeamtagService {

    void insertOrUpdateTag(Team team, String tag) throws ServiceException;

    void addTag(Team team, String tag) throws ServiceException;

    void addTags(Team team, String tags) throws ServiceException;

    void deleteTag(Teamtag teamtag) throws DAOException, ServiceException;
    
}
