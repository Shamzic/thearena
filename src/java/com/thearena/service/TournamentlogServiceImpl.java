/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.service;

import com.thearena.dao.DAOException;
import com.thearena.dao.TournamentlogDAO;
import com.thearena.dao.TournamentlogDAOImpl;
import com.thearena.hibernate.Tournament;
import com.thearena.hibernate.Tournamentlog;
import java.util.Date;
import org.springframework.stereotype.Service;

/**
 *
 * @author Jeanba
 */
@Service
public class TournamentlogServiceImpl implements TournamentlogService {
    TournamentlogDAO tournamentlogDAO;

    public TournamentlogServiceImpl() {
        this.tournamentlogDAO = new TournamentlogDAOImpl();
    }
    
    @Override
    public void addLog(Tournament tournament, String message) throws ServiceException {
        try {
            if(message.isEmpty())
                throw new Exception("Message vide.");
            Tournamentlog log = new Tournamentlog();
            log.setTournament(tournament);
            log.setDeleted(false);
            log.setEntry(message);
            Date now = new Date();
            log.setTime((int)(now.getTime()/1000));
        
            tournamentlogDAO.insertLog(log);
        } catch(DAOException daoException) {
            throw new ServiceException(daoException.getMessage());
        } catch(Exception exception) {
            throw new ServiceException("Une erreur s'est produite pendant la création du log.");
        }
        
    }
}
