/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.beans;

/**
 *
 * @author ASUS PC
 */
public class SearchInput {
    private String type;
    private int player;
    private int captain;
    private int organiser;
    private int game;

    public int getOrganiser() {
        return organiser;
    }

    public void setOrganiser(int organiser) {
        this.organiser = organiser;
    }

    public int getGame() {
        return game;
    }

    public void setGame(int game) {
        this.game = game;
    }

    public int getPlayer() {
        return player;
    }

    public void setPlayer(int player) {
        this.player = player;
    }

    public int getCaptain() {
        return captain;
    }

    public void setCaptain(int captain) {
        this.captain = captain;
    }

    public SearchInput() {}

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
