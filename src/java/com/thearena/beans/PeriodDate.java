/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.beans;

import com.thearena.hibernate.Period;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Jeanba
 */
public class PeriodDate {
    private Date beginDate;
    private Date endDate;    

    public Date getBeginDate() {
        return beginDate;
    }

    public PeriodDate(int beginDate, int endDate) throws Exception {
        setBeginDate(beginDate);
        setEndDate(endDate);
        checkDate();
    }
    
    public PeriodDate(String beginDate, String endDate) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            setBeginDate(formatter.parse(beginDate));
            setEndDate(formatter.parse(endDate));
            checkDate();
        } catch (ParseException ex) {
            throw new Exception("Merci de vérfier le format des dates.");
        }
    }

    public void setBeginDate(Date beginDate) throws Exception {
        
        this.beginDate = beginDate;
        checkDate();
    }
    
    public void setBeginDate(int beginDate) throws Exception {
        this.beginDate = convertTimestampToDate(beginDate);
        checkDate();
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) throws Exception {
        this.endDate = endDate;
        checkDate();
    }
    public void setEndDate(int endDate) throws Exception {
        this.endDate = convertTimestampToDate(endDate);
        checkDate();
    }
    
    public Period getPeriod() {
        Period period = new Period();
        setTimePeriod(period);
        return period;
    }
    public void setTimePeriod(Period period) {
        int timestamp = (int)(beginDate.getTime()/1000);
        period.setStart(timestamp);
        timestamp = (int)(endDate.getTime()/1000);
        period.setEnd(timestamp);
    }
    
    private Date convertTimestampToDate(int dateTime){
        long timestampMs = ((long)dateTime * 1000);
        Timestamp ts = new Timestamp(timestampMs);
        return new Date(ts.getTime());
    }
    
    private void checkDate() throws Exception {
        if (this.beginDate != null && this.endDate != null && this.beginDate.after(endDate))
            throw new Exception("La date de fin doit être supérieure à la fin ");        
    }
    public boolean compare(PeriodDate periodDate) {
        if(this.beginDate == null || this.endDate == null)
            return false;
        if (this.beginDate.after(periodDate.beginDate) && this.endDate.after(periodDate.endDate))
            return true;
        else 
            return false;
    }
}
