/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.beans;

import com.thearena.hibernate.Geek;
import com.thearena.hibernate.RolesGeek;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author shame
 */
public class UtilisateurDetails implements UserDetails  {
    Geek databaseObject;

    
    public Geek getDatabaseObject() {
        return databaseObject;
    }

    public UtilisateurDetails(Geek databaseObject) {
        this.databaseObject = databaseObject;
    }
    public UtilisateurDetails() {
    }

    
    public void setDatabaseObject(Geek databaseObject) {
        this.databaseObject = databaseObject;
    }
    
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> grantedAuths = new ArrayList<>();
        Set<RolesGeek> rolesGeek = this.databaseObject.getRolesGeeks();
        
        rolesGeek.forEach((roleGeek) -> {
            if (!roleGeek.getDeleted())
                grantedAuths.add(new SimpleGrantedAuthority("ROLE_" + roleGeek.getRoles().getRoleId().toUpperCase()));
        });
        
        return grantedAuths;
    }

    @Override
    public String getPassword() {
        return databaseObject.getPassword();
    }

    @Override
    public String getUsername() {
        return databaseObject.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return !databaseObject.getDeleted();
    }

}