/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.security;

import com.thearena.beans.UtilisateurDetails;
import com.thearena.dao.ConnexionDAO;
import com.thearena.dao.ConnexionDAOImpl;
import com.thearena.hibernate.Geek;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 *
 * @author Jeanba
 */
public class CustomAuthentificationProvider implements AuthenticationProvider {

    
    @Override
    public Authentication authenticate(Authentication a) throws AuthenticationException {
        ConnexionDAO connexionDAO = new ConnexionDAOImpl();
        String name = a.getName();
        String password = a.getCredentials().toString();
        String resultat = "";
        // System.out.println(name + ":" + password);
        if (connexionDAO.isPasswordCorresponding(name, password)) {
            Geek connectedGeek = connexionDAO.getGeekByUsername(name);
            UtilisateurDetails user = new UtilisateurDetails(connectedGeek);
            this.setResultatSession(connexionDAO);
            return new UsernamePasswordAuthenticationToken(user, password, user.getAuthorities());
        } else {
            this.setResultatSession(connexionDAO);
            return null;
        }  
    }

    @Override
    public boolean supports(Class<?> type) {
        return type.equals(
          UsernamePasswordAuthenticationToken.class);
    }    
    
    public void setResultatSession(ConnexionDAO connexionDAO) {
            String resultat = connexionDAO.getResultat();
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
            HttpServletRequest request = attributes.getRequest();
            // System.out.println("Resultat : "+resultat);
            HttpSession session= request.getSession(); 
            session.setAttribute("resultat", resultat);
        ;
    }
}

