/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author Jeanba
 */

public class DynamicAuthenticationWrapper implements Authentication {
    private final Authentication wrapped;
    private final List<GrantedAuthority> authorities;

    public DynamicAuthenticationWrapper(Authentication wrapped, Collection<GrantedAuthority> newAuths) {
        this.wrapped = wrapped;
        this.authorities = new ArrayList<>(wrapped.getAuthorities());
        this.authorities.addAll(newAuths);
    }

    public Authentication getWrapped() {
        return wrapped;
    }
    
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public Object getCredentials() {
        return this.wrapped.getCredentials();
    }

    @Override
    public Object getDetails() {
        return this.wrapped.getDetails();
    }

    @Override
    public Object getPrincipal() {
        return this.wrapped.getPrincipal();
    }

    @Override
    public boolean isAuthenticated() {
        return this.wrapped.isAuthenticated();
    }

    @Override
    public void setAuthenticated(boolean bln) throws IllegalArgumentException {
        this.wrapped.setAuthenticated(bln);
    }

    @Override
    public String getName() {
        return this.wrapped.getName();
    }
    
}
