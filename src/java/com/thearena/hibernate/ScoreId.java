package com.thearena.hibernate;
// Generated 29 oct. 2018 14:12:47 by Hibernate Tools 4.3.1



/**
 * ScoreId generated by hbm2java
 */
public class ScoreId  implements java.io.Serializable {


     private int result;
     private String scoreType;

    public ScoreId() {
    }

    public ScoreId(int result, String scoreType) {
       this.result = result;
       this.scoreType = scoreType;
    }
   
    public int getResult() {
        return this.result;
    }
    
    public void setResult(int result) {
        this.result = result;
    }
    public String getScoreType() {
        return this.scoreType;
    }
    
    public void setScoreType(String scoreType) {
        this.scoreType = scoreType;
    }


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof ScoreId) ) return false;
		 ScoreId castOther = ( ScoreId ) other; 
         
		 return (this.getResult()==castOther.getResult())
 && ( (this.getScoreType()==castOther.getScoreType()) || ( this.getScoreType()!=null && castOther.getScoreType()!=null && this.getScoreType().equals(castOther.getScoreType()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + this.getResult();
         result = 37 * result + ( getScoreType() == null ? 0 : this.getScoreType().hashCode() );
         return result;
   }   


}


