package com.thearena.hibernate;
// Generated 29 oct. 2018 14:12:47 by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * Team generated by hbm2java
 */
public class Team  implements java.io.Serializable {


     private String teamId;
     private Geek geek;
     private String name;
     private String tags;
     private Boolean deleted;
     private Set statisticses = new HashSet(0);
     private Set teamGeeks = new HashSet(0);
     private Set versusesForTeam2 = new HashSet(0);
     private Set versusesForTeam1 = new HashSet(0);
     private Set participations = new HashSet(0);
     private Set teamtags = new HashSet(0);
     private Set resultsForLoser = new HashSet(0);
     private Set followteams = new HashSet(0);
     private Set rankings = new HashSet(0);
     private Set resultsForWinner = new HashSet(0);

    public Team() {
    }

	
    public Team(String teamId, Geek geek) {
        this.teamId = teamId;
        this.geek = geek;
    }
    public Team(String teamId, Geek geek, String name, String tags, Boolean deleted, Set statisticses, Set teamGeeks, Set versusesForTeam2, Set versusesForTeam1, Set participations, Set teamtags, Set resultsForLoser, Set followteams, Set rankings, Set resultsForWinner) {
       this.teamId = teamId;
       this.geek = geek;
       this.name = name;
       this.tags = tags;
       this.deleted = deleted;
       this.statisticses = statisticses;
       this.teamGeeks = teamGeeks;
       this.versusesForTeam2 = versusesForTeam2;
       this.versusesForTeam1 = versusesForTeam1;
       this.participations = participations;
       this.teamtags = teamtags;
       this.resultsForLoser = resultsForLoser;
       this.followteams = followteams;
       this.rankings = rankings;
       this.resultsForWinner = resultsForWinner;
    }
   
    public String getTeamId() {
        return this.teamId;
    }
    
    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }
    public Geek getGeek() {
        return this.geek;
    }
    
    public void setGeek(Geek geek) {
        this.geek = geek;
    }
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public String getTags() {
        return this.tags;
    }
    
    public void setTags(String tags) {
        this.tags = tags;
    }
    public Boolean getDeleted() {
        return this.deleted;
    }
    
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
    public Set getStatisticses() {
        return this.statisticses;
    }
    
    public void setStatisticses(Set statisticses) {
        this.statisticses = statisticses;
    }
    public Set getTeamGeeks() {
        return this.teamGeeks;
    }
    
    public void setTeamGeeks(Set teamGeeks) {
        this.teamGeeks = teamGeeks;
    }
    public Set getVersusesForTeam2() {
        return this.versusesForTeam2;
    }
    
    public void setVersusesForTeam2(Set versusesForTeam2) {
        this.versusesForTeam2 = versusesForTeam2;
    }
    public Set getVersusesForTeam1() {
        return this.versusesForTeam1;
    }
    
    public void setVersusesForTeam1(Set versusesForTeam1) {
        this.versusesForTeam1 = versusesForTeam1;
    }
    public Set getParticipations() {
        return this.participations;
    }
    
    public void setParticipations(Set participations) {
        this.participations = participations;
    }
    public Set getTeamtags() {
        return this.teamtags;
    }
    
    public void setTeamtags(Set teamtags) {
        this.teamtags = teamtags;
    }
    public Set getResultsForLoser() {
        return this.resultsForLoser;
    }
    
    public void setResultsForLoser(Set resultsForLoser) {
        this.resultsForLoser = resultsForLoser;
    }
    public Set getFollowteams() {
        return this.followteams;
    }
    
    public void setFollowteams(Set followteams) {
        this.followteams = followteams;
    }
    public Set getRankings() {
        return this.rankings;
    }
    
    public void setRankings(Set rankings) {
        this.rankings = rankings;
    }
    public Set getResultsForWinner() {
        return this.resultsForWinner;
    }
    
    public void setResultsForWinner(Set resultsForWinner) {
        this.resultsForWinner = resultsForWinner;
    }




}


