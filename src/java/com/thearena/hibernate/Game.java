package com.thearena.hibernate;
// Generated 29 oct. 2018 14:12:47 by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * Game generated by hbm2java
 */
public class Game  implements java.io.Serializable {


     private Integer gameId;
     private Gametype gametype;
     private String name;
     private Integer ageLimit;
     private Boolean deleted;
     private Set statisticses = new HashSet(0);
     private Set followgames = new HashSet(0);
     private Set tournaments = new HashSet(0);
     private Set rankings = new HashSet(0);
     private Set divisions = new HashSet(0);

    public Game() {
    }

	
    public Game(Gametype gametype) {
        this.gametype = gametype;
    }
    public Game(Gametype gametype, String name, Integer ageLimit, Boolean deleted, Set statisticses, Set followgames, Set tournaments, Set rankings, Set divisions) {
       this.gametype = gametype;
       this.name = name;
       this.ageLimit = ageLimit;
       this.deleted = deleted;
       this.statisticses = statisticses;
       this.followgames = followgames;
       this.tournaments = tournaments;
       this.rankings = rankings;
       this.divisions = divisions;
    }
   
    public Integer getGameId() {
        return this.gameId;
    }
    
    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }
    public Gametype getGametype() {
        return this.gametype;
    }
    
    public void setGametype(Gametype gametype) {
        this.gametype = gametype;
    }
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public Integer getAgeLimit() {
        return this.ageLimit;
    }
    
    public void setAgeLimit(Integer ageLimit) {
        this.ageLimit = ageLimit;
    }
    public Boolean getDeleted() {
        return this.deleted;
    }
    
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
    public Set getStatisticses() {
        return this.statisticses;
    }
    
    public void setStatisticses(Set statisticses) {
        this.statisticses = statisticses;
    }
    public Set getFollowgames() {
        return this.followgames;
    }
    
    public void setFollowgames(Set followgames) {
        this.followgames = followgames;
    }
    public Set getTournaments() {
        return this.tournaments;
    }
    
    public void setTournaments(Set tournaments) {
        this.tournaments = tournaments;
    }
    public Set getRankings() {
        return this.rankings;
    }
    
    public void setRankings(Set rankings) {
        this.rankings = rankings;
    }
    public Set getDivisions() {
        return this.divisions;
    }
    
    public void setDivisions(Set divisions) {
        this.divisions = divisions;
    }




}


