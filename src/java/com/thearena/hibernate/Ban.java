package com.thearena.hibernate;
// Generated 29 oct. 2018 14:12:47 by Hibernate Tools 4.3.1



/**
 * Ban generated by hbm2java
 */
public class Ban  implements java.io.Serializable {


     private Integer banId;
     private Geek geek;
     private Period period;
     private Reason reason;
     private String commentary;
     private Boolean deleted;

    public Ban() {
    }

	
    public Ban(Geek geek, Period period, Reason reason) {
        this.geek = geek;
        this.period = period;
        this.reason = reason;
    }
    public Ban(Geek geek, Period period, Reason reason, String commentary, Boolean deleted) {
       this.geek = geek;
       this.period = period;
       this.reason = reason;
       this.commentary = commentary;
       this.deleted = deleted;
    }
   
    public Integer getBanId() {
        return this.banId;
    }
    
    public void setBanId(Integer banId) {
        this.banId = banId;
    }
    public Geek getGeek() {
        return this.geek;
    }
    
    public void setGeek(Geek geek) {
        this.geek = geek;
    }
    public Period getPeriod() {
        return this.period;
    }
    
    public void setPeriod(Period period) {
        this.period = period;
    }
    public Reason getReason() {
        return this.reason;
    }
    
    public void setReason(Reason reason) {
        this.reason = reason;
    }
    public String getCommentary() {
        return this.commentary;
    }
    
    public void setCommentary(String commentary) {
        this.commentary = commentary;
    }
    public Boolean getDeleted() {
        return this.deleted;
    }
    
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }




}


