package com.thearena.hibernate;
// Generated 29 oct. 2018 14:12:47 by Hibernate Tools 4.3.1



/**
 * TeamGeekId generated by hbm2java
 */
public class TeamGeekId  implements java.io.Serializable {


     private int player;
     private String team;

    public TeamGeekId() {
    }

    public TeamGeekId(int player, String team) {
       this.player = player;
       this.team = team;
    }
   
    public int getPlayer() {
        return this.player;
    }
    
    public void setPlayer(int player) {
        this.player = player;
    }
    public String getTeam() {
        return this.team;
    }
    
    public void setTeam(String team) {
        this.team = team;
    }


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof TeamGeekId) ) return false;
		 TeamGeekId castOther = ( TeamGeekId ) other; 
         
		 return (this.getPlayer()==castOther.getPlayer())
 && ( (this.getTeam()==castOther.getTeam()) || ( this.getTeam()!=null && castOther.getTeam()!=null && this.getTeam().equals(castOther.getTeam()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + this.getPlayer();
         result = 37 * result + ( getTeam() == null ? 0 : this.getTeam().hashCode() );
         return result;
   }   


}


