/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.controller;

import com.thearena.dao.InscriptionDAO;
import com.thearena.dao.InscriptionDAOImpl;
import com.thearena.dao.TeamDAOImpl;
import com.thearena.hibernate.Geek;
import com.thearena.hibernate.Team;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author shame
 */
@Controller
public class InscriptionController {
    @RequestMapping(value = "/inscription", method = RequestMethod.GET)
    public String geekCreate(Model model) {
        return "inscription"; // Lien vers la jsp
    }
    
    @RequestMapping(value = "/inscription", method = RequestMethod.POST)
    public String geekCreate(@Validated Geek g, String pass, HttpSession session, Model model) {
        InscriptionDAO inscriptionDAO = new InscriptionDAOImpl();
        g.setDeleted(Boolean.FALSE);
        String usernameEntered = g.getUsername();
        String pass1 = g.getPassword();
        String mail = g.getMail();
        String echecInscription = "";
        String succesInscription = "";
        
        if(inscriptionDAO.canGeekBeAdded(g, usernameEntered, pass1, pass)){
            session.removeAttribute("echecUserName");
            session.removeAttribute("echecPassword");
            inscriptionDAO.addGeek(g);
            succesInscription = "Inscription réussie! Bienvenue "+g.getUsername()+
                ", veuillez vous connecter pour accéder à toutes les fonctionnalités de la plateforme";
        } else {
            
            echecInscription = "L'inscription a échouée";
            
            if(inscriptionDAO.getAlreadyRegisteredMessage()!=null)
                session.setAttribute("echecUserName", inscriptionDAO.getAlreadyRegisteredMessage());
            else
                session.removeAttribute("echecUserName");
            if(inscriptionDAO.getPasswordMessage()!=null)
                session.setAttribute("echecPassword", inscriptionDAO.getPasswordMessage());
            else
                session.removeAttribute("echecPassword");
        }
        //System.out.println("Resultat Inscription : "+inscriptionDAO.getAlreadyRegisteredMessage());
        session.setAttribute("geekSession",g);
        session.setAttribute("succesInscription",succesInscription);
        session.setAttribute("echecInscription",echecInscription);  
        
        return "redirect:/inscription";
    }
}
