/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.controller;

import com.thearena.dao.ConnexionDAO;
import com.thearena.dao.ConnexionDAOImpl;
import com.thearena.dao.InscriptionDAO;
import com.thearena.dao.InscriptionDAOImpl;
import com.thearena.dao.TeamDAOImpl;
import com.thearena.hibernate.Geek;
import com.thearena.hibernate.Team;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author shame
 */
@Controller
public class ConnexionController {
    @RequestMapping(value = "/connexion", method = RequestMethod.GET)
    public String geekCreate(Model model) {
        return "connexion"; // Lien vers la jsp
    }
    
    @RequestMapping(value = "/connexion", method = RequestMethod.POST)
    public String geekCreate(@Validated Geek g, String pass, HttpSession session, Model model) {
        ConnexionDAO connexionDAO = new ConnexionDAOImpl();
        g.setDeleted(Boolean.FALSE);
        String path = "";
        String usernameEntered = g.getUsername(); 
        if(connexionDAO.isPasswordCorresponding(usernameEntered, pass)) {       
           session.setAttribute("geekSession",g);
        }
        String resultat = connexionDAO.getResultat();
        System.out.println("Resultat : "+resultat);
        session.setAttribute("resultat", resultat);
        path = "redirect:/connexion"; // redirection accueil une fois 
        return path;                       // l'inscription validée
    }
}
