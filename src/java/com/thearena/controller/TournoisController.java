/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.controller;

import com.thearena.beans.PeriodDate;
import com.thearena.dao.GameDAO;
import com.thearena.dao.GameDAOImpl;
import com.thearena.hibernate.Game;
import com.thearena.hibernate.Participation;
import com.thearena.hibernate.Team;
import com.thearena.hibernate.Tournament;
import com.thearena.hibernate.Tournamenttag;
import com.thearena.service.ParticipationService;
import com.thearena.service.ParticipationServiceImpl;
import com.thearena.service.ServiceException;
import com.thearena.service.TournamentService;
import com.thearena.service.TournamentServiceImpl;
import com.thearena.service.TournamentlogService;
import com.thearena.service.TournamentlogServiceImpl;
import com.thearena.service.TournamenttagServiceImpl;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Jeanba
 */
@Controller
public class TournoisController {
    TournamentlogService tournamentlogService = new TournamentlogServiceImpl();
    TournamentService tournamentService = new TournamentServiceImpl();
    ParticipationService participationService = new ParticipationServiceImpl();
    
    @RequestMapping(value = "/tournois", method = RequestMethod.GET)
    public String tournois(Model model) {
        try {
          List<Tournament> tournois = tournamentService.selectTournaments();
            model.addAttribute("tournois", tournois);  
        } catch (ServiceException ex) {
            model.addAttribute("errorMsg", ex.getMessage());
        }        
        return "tournois";
    }

    
    @RequestMapping(value = "/tournois/{tournamentId}", method = RequestMethod.GET)
    public String tournoisDetail(@PathVariable(value="tournamentId") String id, Model model) {   
        try {
            Tournament tournoi = tournamentService.getTournamentById(id);
            if (tournoi.getDeleted())
                throw new Exception("Ce tournoi n'existe plus.");
            model.addAttribute("tournoi", tournoi);
            model.addAttribute("registrationPeriod", new PeriodDate(tournoi.getPeriodByRegisteringPeriod().getStart(), tournoi.getPeriodByRegisteringPeriod().getEnd()));
            model.addAttribute("playingPeriod", new PeriodDate(tournoi.getPeriodByPlayingPeriod().getStart(), tournoi.getPeriodByPlayingPeriod().getEnd()));
            return "tournoisDetail";
        } catch(Exception ex) {
            model.addAttribute("errorMsg", ex.getMessage());
            return "redirect:/tournois";
        }
        
        
    }
    
    @RequestMapping(value = "/tournois/{tournamentId}/edit", method = RequestMethod.GET)
    public String tournoisEdit(@PathVariable(value="tournamentId") String id, Model model) {
        try {
            Tournament tournoi = tournamentService.getTournamentById(id);
            if (!tournamentService.checkAuthorisationToUpdate(tournoi))
                throw new ServiceException("Vous n'avez pas l'autorisation d'acceder a cette page.");
            model.addAttribute("tournoi", tournoi);
            model.addAttribute("registrationPeriod", new PeriodDate(tournoi.getPeriodByRegisteringPeriod().getStart(), tournoi.getPeriodByRegisteringPeriod().getEnd()));
            model.addAttribute("playingPeriod", new PeriodDate(tournoi.getPeriodByPlayingPeriod().getStart(), tournoi.getPeriodByPlayingPeriod().getEnd()));
            return "tournoisForm";
        } catch (Exception ex) {           
            model.addAttribute("errorMsg", ex.getMessage());
            return "redirect:/tournois/"  + id;
        }
    }
    
    
    @RequestMapping(value = "/tournois/{tournamentId}/edit", method = RequestMethod.POST)
    public String tournoisEdit(@PathVariable(value="tournamentId") String id,
            @Validated Tournament tournament,
            String registeringPeriodStart,
            String registeringPeriodEnd,
            String playingPeriodStart,
            String playingPeriodEnd,            
            Model model) {
        try {
            tournament.setTournamentId(id);
            
            tournamentService.checkAuthorisationToUpdate(id);
            tournamentService.updateTournament(tournament, new PeriodDate(registeringPeriodStart, registeringPeriodEnd), new PeriodDate(playingPeriodStart, playingPeriodEnd));
            return "redirect:/tournois/" + id; 
        } catch (Exception ex) {
            model.addAttribute("errorMsg", ex.getMessage());
            return "redirect:/tournois/" + id;
        }
        
    }
    
    @RequestMapping(value = "/tournois/create", method = RequestMethod.GET)
    public String tournoisCreate(Model model) {
        GameDAO gameDAO = new GameDAOImpl();
        List<Game> games = gameDAO.listGame();
        model.addAttribute("games", games);
        return "tournoisForm";
    }
    
    @RequestMapping(value = "/tournois/create", method = RequestMethod.POST)
    public String tournoisCreate(@Validated Tournament tournament,
            String playingPeriodStart,
            String playingPeriodEnd,
            String registeringPeriodStart,
            String registeringPeriodEnd,
            Model model) {
        try {
            PeriodDate playingPeriod = new PeriodDate(playingPeriodStart, playingPeriodEnd);
            PeriodDate registeringPeriod = new PeriodDate(registeringPeriodStart, registeringPeriodEnd);
              
            tournamentService.createTournament(tournament, registeringPeriod, playingPeriod);
            return "redirect:/tournois/" + tournament.getTournamentId() ;
        } catch (Exception ex) {
            model.addAttribute("errorMsg", ex.getMessage());
            return "redirect:/tournois/create";
        }
    }

    @RequestMapping(value = "/tournois/{tournamentId}/delete", method = RequestMethod.GET)
    public String tournoiDelete(@PathVariable(value="tournamentId") String id, Model model) {
        try {
          tournamentService.deleteTournament(id);  
        } catch (ServiceException ex) {
            model.addAttribute("errorMsg", ex.getMessage());
        }
        
        return "redirect:/tournois/";
    }
    
    @RequestMapping(value = "/tournois/{tournamentId}/participate", method = RequestMethod.POST)
    public String tournoiParticipate(@PathVariable(value="tournamentId") String id, @Validated Team team, Model model) {
        try {
            
        Tournament tournament = new Tournament();
        tournament.setTournamentId(id);
        participationService.addParticipation(tournament, team);        
        return "redirect:/tournois/" + id;
        }
        catch (ServiceException ex) {
            return "redirect:/tournois/" + id;
        }
    }
    
    @RequestMapping(value = "/tournois/{tournamentId}/updateParticipation", method = RequestMethod.POST)
    public String tournoiParticipateUpdate(@PathVariable(value="tournamentId") String id, @Validated Participation participation, Model model) {
        try {
        participationService.updateParticipation(participation, id);
        return "redirect:/tournois/" + id;
        }
        catch (ServiceException ex) {
            model.addAttribute("errorMsg", ex.getMessage());
            return "redirect:/tournois/" + id;
        }
    }
    
    @RequestMapping(value = "/tournois/{tournamentId}/leave", method = RequestMethod.POST)
    public String tournoiLeave(@PathVariable(value="tournamentId") String id, @Validated Team team, Model model) {
        try {
            participationService.deleteParticipation(id, team);
        } catch (ServiceException ex) {
            model.addAttribute("errorMsg", ex.getMessage());
        }       

        return "redirect:/tournois/" + id;
    }
    @RequestMapping(value = "/tournois/{tournamentId}/deleteTag", method = RequestMethod.POST)
    public String tournoiDeleteTag(@PathVariable(value="tournamentId") String id, @Validated Tournamenttag tag, Model model) {
        try {
            TournamenttagServiceImpl tournamentTagService = new TournamenttagServiceImpl();
            Tournament tournament = new Tournament();
            tournament.setTournamentId(id);
            tournamentTagService.deleteTag(tag.getTagId());  
            tournamentlogService.addLog(tournament, "Le tag ID " + tag.getTagId() + " a Ã©tÃ© supprimÃ©.");
        } catch (ServiceException ex) {
            model.addAttribute("errorMsg", ex.getMessage());
        }       

        return "redirect:/tournois/" + id;
    }
    @RequestMapping(value = "/tournois/{tournamentId}/addTag", method = RequestMethod.POST)
    public String tournoiAddTag(@PathVariable(value="tournamentId") String id, @Validated String tag, Model model) {
        try {
            TournamenttagServiceImpl tournamentTagService = new TournamenttagServiceImpl();
            Tournament tournament = new Tournament();
            tournament.setTournamentId(id);
            tournamentTagService.addOrUpdateTag(tournament, tag);  
            tournamentlogService.addLog(tournament, "Le tag " + tag + " a Ã©tÃ© ajoutÃ©.");
        } catch (ServiceException ex) {
            model.addAttribute("errorMsg", ex.getMessage());
        }       

        return "redirect:/tournois/" + id;
    }
}
