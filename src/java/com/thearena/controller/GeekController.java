/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.controller;

import com.thearena.dao.GeekConnexionDAOImpl;
import com.thearena.dao.GeekDAOImpl;
import com.thearena.hibernate.Geek;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class GeekController {
    @RequestMapping(value = "/geek", method = RequestMethod.GET)
   public String geek(String username, Model model) {
        GeekConnexionDAOImpl geekConnexionDAO = new GeekConnexionDAOImpl();
        List<Geek> geeks = geekConnexionDAO.listGeeks();
        model.addAttribute("geek", geeks);
        return "geek";
    }
    @RequestMapping(value = "/geek/{username}/edit", method = RequestMethod.GET)
    public String geekEdit(@PathVariable(value="username") String username, Model model) {
        System.out.println(username);
        GeekDAOImpl geekDAO = new GeekDAOImpl();
        Geek geek = geekDAO.getGeekByUsername(username);
        System.out.println(" GEEK USERNAME : "+ geek.getUsername());
        model.addAttribute("geek", geek);
        return "geekParameter";
    }
    
    
    @RequestMapping(value = "/geek/{username}/edit", method = RequestMethod.POST)
    public String geekEdit(@PathVariable(value="username") String username, @Validated Geek geek,  Model model) {
        
        geek.setDeleted(Boolean.FALSE); // A faire car sinon le champ est null
        GeekDAOImpl geekDAO = new GeekDAOImpl();
        geekDAO.updateGeek(geek);
        String path = "redirect:/geek";
        return path;
    }
}
