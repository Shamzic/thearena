/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.controller;

import com.thearena.beans.SearchInput;
import com.thearena.dao.DAOException;
import com.thearena.dao.RolesGeekDAO;
import com.thearena.dao.RolesGeekDAOImpl;
import com.thearena.dao.SearchDAO;
import com.thearena.dao.SearchDAOImpl;
import com.thearena.dao.TeamDAO;
import com.thearena.dao.TeamDAOImpl;
import com.thearena.dao.TournamentDAO;
import com.thearena.dao.TournamentDAOImpl;
import com.thearena.hibernate.Game;
import com.thearena.hibernate.Geek;
import com.thearena.hibernate.Team;
import com.thearena.service.ServiceException;
import com.thearena.service.TeamService;
import com.thearena.service.TeamServiceImpl;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author ASUS PC
 */


@Controller
public class SearchController {
    
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String search(@RequestParam("searchTerms") String searchItems, Model model) {
        List<String> search = Arrays.asList(searchItems.split("\\s|\\+"));
        model.addAttribute("search", search);
        
        SearchDAO searchDAO = new SearchDAOImpl();
        List<String> to = new ArrayList<>();
        List<String> te = new ArrayList<>();
        List<String> temp;
        for (String tag : search){
            temp = searchDAO.getTournamentsByTag(tag);
            if (temp != null) {
                to.addAll(searchDAO.getTournamentsByTag(tag));
            }
            temp = searchDAO.getTeamsByTag(tag);
            if (temp != null) {
                te.addAll(searchDAO.getTeamsByTag(tag));
            }
            
        }
        
        // remove duplicates
        Set<String> tournaments = new HashSet<>();
        Set<String> teams = new HashSet<>();
        tournaments.addAll(to);
        teams.addAll(te);
        
        TournamentDAO tournamentDAO = new TournamentDAOImpl();
        TeamDAO teamDAO = new TeamDAOImpl();
        
        // send results
        model.addAttribute("tournaments", tournaments);
        model.addAttribute("teams", teams);
        model.addAttribute("tournamentDAO", tournamentDAO);
        model.addAttribute("teamDAO", teamDAO);

        return "searchResults";
    }
    
    @RequestMapping(value = "/advancedSearch", method = RequestMethod.GET)
    public String advancedSearch(Model model){
        model.addAttribute("searchInput", new SearchInput());
        
        RolesGeekDAO rolesGeekDAO = new RolesGeekDAOImpl();
        List<Geek> players = rolesGeekDAO.getGeeksByRole("Player");
        List<Geek> captains = rolesGeekDAO.getGeeksByRole("Captain");
        List<Geek> organisers = rolesGeekDAO.getGeeksByRole("Organiser");
        model.addAttribute("players", players);
        model.addAttribute("captains", captains);
        model.addAttribute("organisers", organisers);
              
        TournamentDAO tournamentDAO = new TournamentDAOImpl();
        List<Game> games;
        try {
            games = tournamentDAO.getGamesPlayed();
            model.addAttribute("games", games);
        } catch (DAOException ex) {
            Logger.getLogger(SearchController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "advancedSearch";
    }
    
    @RequestMapping(value = "/advancedSearchResult", method = RequestMethod.GET)
    public String advancedSearchResult(@ModelAttribute("searchInput") SearchInput input, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "advancedSearch";
        }
        
        SearchDAO searchDAO = new SearchDAOImpl();
        List<String> results = new ArrayList<String>();
        
        if ("team".equals(input.getType())){
            if (input.getCaptain() != 0){
                if (input.getPlayer() != 0){
                    results = searchDAO.teamSearch(input.getCaptain(), input.getPlayer());
                } else {
                    results = searchDAO.captainSearch(input.getPlayer());
                }
            } else {
                if (input.getPlayer() != 0){
                    results = searchDAO.playerSearch(input.getPlayer());
                } else {
                    return "advancedSearch";
                }
            }
        } else {
            if (input.getOrganiser() != 0){
                if (input.getGame() != 0){
                    results = searchDAO.tournamentSearch(input.getOrganiser(), input.getGame());
                } else {
                    results = searchDAO.organiserSearch(input.getOrganiser());
                }
            } else {
                if (input.getGame() != 0){
                    results = searchDAO.gameSearch(input.getGame());
                } else {
                    return "advancedSearch";
                }
            }            
        }
        
        TournamentDAO tournamentDAO = new TournamentDAOImpl();
        TeamDAO teamDAO = new TeamDAOImpl();
        
        model.addAttribute("type", input.getType());
        model.addAttribute("results", results);
        model.addAttribute("tournamentDAO", tournamentDAO);
        model.addAttribute("teamDAO", teamDAO);
        
        return "advancedSearchResult";
    }
}
