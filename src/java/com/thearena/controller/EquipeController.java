/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thearena.controller;
import com.thearena.beans.UtilisateurDetails;
import com.thearena.dao.DAOException;
import com.thearena.dao.ParticipationDAO;
import com.thearena.dao.ParticipationDAOImpl;
import com.thearena.hibernate.Geek;
import com.thearena.hibernate.ParticipationId;
import com.thearena.hibernate.Roles;
import com.thearena.hibernate.TeamGeekId;
import com.thearena.hibernate.TeamGeek;
import com.thearena.hibernate.Team;
import com.thearena.hibernate.Teamtag;
import com.thearena.hibernate.Tournament;
import com.thearena.service.ServiceException;
import com.thearena.service.TeamGeekService;
import com.thearena.service.TeamGeekServiceImpl;
import com.thearena.service.TeamService;
import com.thearena.service.TeamServiceImpl;
import com.thearena.service.TeamtagService;
import com.thearena.service.TeamtagServiceImpl;
import com.thearena.service.TournamentlogService;
import com.thearena.service.TournamentlogServiceImpl;
import java.util.List;
import java.util.UUID;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author nhuch
 */
@Controller
public class EquipeController {
    
    
    @RequestMapping(value = "/equipe", method = RequestMethod.GET)
    public String equipe(Model model) throws ServiceException {
        try{
            TeamService teamService = new TeamServiceImpl();
            List<Team> equipe = teamService.listTeam();
            model.addAttribute("equipe", equipe);
            return "equipe";
        }catch(Exception ex){
            model.addAttribute("errorMsg", ex.getMessage());
        }
        return "redirect:/equipe";
    }
    
    @RequestMapping(value = "/equipe/{teamId}", method = RequestMethod.GET)
    public String equipeDetail(@PathVariable(value="teamId") String id, Model model) throws ServiceException {
        try{
            TeamService teamService = new TeamServiceImpl();
            Team equipe = teamService.getTeamById(id);
            model.addAttribute("membres", equipe.getTeamGeeks());
            model.addAttribute("equipe", equipe);
            model.addAttribute("tournois", equipe.getParticipations());
            return "equipeDetail";
        }catch(Exception ex){
            model.addAttribute("errorMsg", ex.getMessage());
            return "redirect:/equipe";
        }
    }
    
    @RequestMapping(value="/equipe/{teamId}/edit",method = RequestMethod.GET)
    public String equipeEdition(@PathVariable(value="teamId") String id, Model model) throws ServiceException{
        try{
            TeamService teamService = new TeamServiceImpl();
            Team equipe = teamService.getTeamById(id);
            model.addAttribute("membres", equipe.getTeamGeeks());
            model.addAttribute("equipe", equipe);
            model.addAttribute("tournois", equipe.getParticipations());
            return "equipeEdition";
        }catch(Exception ex){
            model.addAttribute("errorMsg", ex.getMessage());
            return "redirect:/equipe";
        }
    }   
    
        @RequestMapping(value = "/equipe/{teamId}/edit", method = RequestMethod.POST)
    public String equipeEditition(@PathVariable(value="teamId") String id,@Validated Team team,Model model) throws ServiceException {
        try{
            TeamService teamService = new TeamServiceImpl();
            team.setGeek(teamService.getTeamByIdBis(id).getGeek());
            team.setTags(teamService.getTeamByIdBis(id).getTags());
            team.setDeleted(teamService.getTeamByIdBis(id).getDeleted());
            
            teamService.updateTeam(team);
            String path = "redirect:/equipe/"+team.getTeamId();
            return path;
        }
        catch(Exception ex){
            model.addAttribute("errorMsg", ex.getMessage());
            return "redirect:/equipe/"+team.getTeamId()+"/edit";
        }
        
    }
    
    @RequestMapping(value = "/equipe/create", method = RequestMethod.GET)
    public String equipeCreate(Model model) {
        return "equipeCreation";
    }
    
    @RequestMapping(value = "/equipe/create", method = RequestMethod.POST)
    public String equipeCreate(@Validated Team team, Model model) throws ServiceException {
        try{
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();      
            UtilisateurDetails user = (UtilisateurDetails)auth.getPrincipal();
            team.setDeleted(Boolean.FALSE);
            team.setGeek(user.getDatabaseObject());
            UUID uuid = UUID.randomUUID();
            team.setTeamId(uuid.toString());
            TeamGeekId teamGeekId = new TeamGeekId(team.getGeek().getUserId(),team.getTeamId());
            TeamGeek teamGeek = new TeamGeek(teamGeekId,user.getDatabaseObject(),team,false);
            TeamService teamService = new TeamServiceImpl();
            teamService.verificationTeamId(team);
            teamService.addTeam(team, teamGeek);
            String path = "redirect:/equipe/"+team.getTeamId();
            return path;
        }
        catch(Exception ex){
            model.addAttribute("errorMsg", ex.getMessage());
            return "redirect:/equipe/create";
        }
    }
    
    @RequestMapping(value = "/equipe/{teamId}/delete", method = RequestMethod.POST)
    public String equipeDelete(@PathVariable(value="teamId") String id, Model model) throws DAOException, ServiceException {
        try{
            TeamService teamService = new TeamServiceImpl();
            teamService.deleteTeam(id);  
            return "redirect:/equipe";
        }
        catch(Exception ex){
            model.addAttribute("errorMsg", ex.getMessage());
            return "redirect:/equipe/{teamId}";
        }
    }
    
    @RequestMapping(value = "/equipe/{teamId}/join", method = RequestMethod.POST)
    public String equipeInscription(@PathVariable(value="teamId") String id, @Validated Geek geek,Model model) throws ServiceException, DAOException {
        try{
            TeamService teamService = new TeamServiceImpl();
            Team team = teamService.getTeamById(id);
            TeamGeekId teamGeekId = new TeamGeekId(geek.getUserId(),team.getTeamId());
            TeamGeek teamGeek = new TeamGeek(teamGeekId,geek,team,false);
            TeamGeekService teamGeekService = new TeamGeekServiceImpl();
            teamGeekService.addOrUpdateTeamGeek(teamGeek);
            return "redirect:/equipe/{teamId}";
        }
        catch(Exception ex){
            model.addAttribute("errorMsg", ex.getMessage());
            return "redirect:/equipe/{teamId}";
        }
    }
    
    @RequestMapping(value = "/equipe/{teamId}/leave", method = RequestMethod.POST)
    public String equipeDesinscription(@PathVariable(value="teamId") String id,@Validated Geek geek,Model model) throws ServiceException, DAOException {
        try{
            TeamService teamService = new TeamServiceImpl();
            Team team = teamService.getTeamById(id);
            TeamGeekId teamGeekId = new TeamGeekId(geek.getUserId(),team.getTeamId());
            TeamGeek teamGeek = new TeamGeek(teamGeekId,geek,team,true);
            TeamGeekService teamGeekService = new TeamGeekServiceImpl();
            teamGeekService.updateTeamGeek(teamGeek);
            return "redirect:/equipe/{teamId}";
        }
        catch(Exception ex){
            model.addAttribute("errorMsg", ex.getMessage());
            return "redirect:/equipe/{teamId}";
        }
    }
    
    @RequestMapping(value = "/equipe/{teamId}/ff", method = RequestMethod.POST)
    public String EquipeLeave(@PathVariable(value="teamId") String teamid,@Validated Tournament tournament, Model model) {
        try {
            TournamentlogService tournamentlogService = new TournamentlogServiceImpl();
            ParticipationDAO participationDAO = new ParticipationDAOImpl();
            Team team = new Team();
            team.setTeamId(teamid);
            participationDAO.deleteParticipation(new ParticipationId(teamid, tournament.getTournamentId()));  
            tournamentlogService.addLog(tournament, "La participation de l'équipe " + teamid + " a été supprimée.");
        } catch (Exception ex) {
            model.addAttribute("errorMsg", ex.getMessage());
        }       
        return "redirect:/equipe/" + teamid;
    }
    
    @RequestMapping(value= "/equipe/{teamId}/suppr", method = RequestMethod.POST)
    public String EquipeTagDelete(@Validated Teamtag teamtag,@PathVariable(value="teamId") String teamid, Model model) throws DAOException, ServiceException {
        try{
            TeamtagService teamtagService = new TeamtagServiceImpl();
            teamtagService.deleteTag(teamtag);
            return "redirect:/equipe/" + teamid +"/edit";
        }
        catch(Exception ex){
            model.addAttribute("errorMsg", ex.getMessage());
            return "redirect:/equipe/{teamId}/edit";
        }
    }
    
    @RequestMapping(value= "/equipe/{teamId}/add", method = RequestMethod.POST)
    public String EquipeTagAdd(@Validated Teamtag teamtag,@PathVariable(value="teamId") String teamid, Model model) throws DAOException, ServiceException {
        try{
            TeamService teamService = new TeamServiceImpl();
            teamtag.setTeam(teamService.getTeamById(teamid));
            TeamtagService teamtagService = new TeamtagServiceImpl();
            teamtagService.insertOrUpdateTag(teamtag.getTeam(), teamtag.getTag());
            return "redirect:/equipe/" + teamid +"/edit";
        }
        catch(Exception ex){
            model.addAttribute("errorMsg", ex.getMessage());
            return "redirect:/equipe/{teamId}/edit";
        }
    }
}
