<%-- 
    Document   : connexion
    Created on : 24 oct. 2018, 22:55:37
    Author     : shame
--%>

<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Connexion</title>
        <link type="text/css" rel="stylesheet" href="ressources/CSS/form.css"  />
        <%@ include file="header.jsp" %>
    </head>
    <body>
       <%@ include file="menu.jsp" %>
        <form:form method="post" action="login_auth" class="main-container">
            <fieldset class="formulaire-inscription">
                <legend>Connexion</legend>
                <p>Vous pouvez vous connecter via ce formulaire.</p>

                <label for="username">Nom utilisateur <span class="requis">*</span></label>
                <input type="text" id="username" name="username" value="${geek.username}" size="20" maxlength="60" />
                <br />

                <label for="pass">Mot de passe <span class="requis">*</span></label>
                <input type="password" id="password" name="password" value="${pass}" size="20" maxlength="20" />
                <br />

                <input type="submit" value="Connexion" class="sansLabel" />
                <br />
                
                <p style="color: red;"> <c:out value="${sessionScope.resultat}" /></p>
            </fieldset>
        </form:form>
    </body>
</html>