<%-- 
    Document   : searchResults
    Created on : 29 oct. 2018, 12:02:31
    Author     : ASUS PC
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="header.jsp" %>
        <title>Recherche</title>
    </head>
    <body>
        <%@ include file="menu.jsp" %>
        <div class="container main-container pt-3 pb-3 h-100">
        
        <div class = "col-sm-12"><c:choose>
                
            <c:when test = "${!empty teams || !empty tournaments}">
                <p style="color:gray;">
                    <b>Résultats de la recherche pour les termes suivants :</b>
                    <c:forEach items="${search}" var="item">
                        ${item} 
                    </c:forEach>
                </p> 
            
                <c:if test = "${!empty teams}">
                    <h3>Equipes : </h3>
                    <ul>
                        <c:forEach items="${teams}" var="team">
                            <li><a href="equipe/${team}">${teamDAO.getTeamById(team).name}</a></li>
                        </c:forEach>
                    </ul>
                </c:if>

                <c:if test = "${!empty tournaments}">
                    <h3>Tournois : </h3>
                    <ul>
                        <c:forEach items="${tournaments}" var="tournament">
                            <li><a href="tournois/${tournament}">${tournamentDAO.getTournamentById(tournament).name}</a></li>
                        </c:forEach>
                    </ul>
                </c:if>
            </c:when>
                    
            <c:otherwise>
                <p style="color:gray;">
                    <b>Aucun résultat pour les termes suivants :</b>
                    <c:forEach items="${search}" var="item">
                        ${item} 
                    </c:forEach>
                </p>    
            </c:otherwise>

        </c:choose></div>
    </body>
</html>
