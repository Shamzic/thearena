<%-- 
    Document   : advancedSearch
    Created on : 1 nov. 2018, 08:15:55
    Author     : ASUS PC
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="header.jsp" %>
        <title>Recherche avanc�e</title>
        
        <script>
            function updateDisplay() {
                var val = document.getElementById('type').options[document.getElementById('type').selectedIndex].value;
                if (val === 'team'){
                    document.getElementById('teamSpecific').style.display = "block";
                    document.getElementById('tournamentSpecific').style.display = "none";
                } else if (val === 'tournament') {
                    document.getElementById('teamSpecific').style.display = "none";
                    document.getElementById('tournamentSpecific').style.display = "block";
                }
            }
            window.onload = function() {
                updateDisplay();
            };
        </script>
    </head>
    <body>
        <%@ include file="menu.jsp" %>
        <div class="container main-container pt-3 pb-3 h-100">
        
        <div class = "col-sm-12">
            <form:form modelAttribute="searchInput" action="advancedSearchResult" method="get" onchange="updateDisplay()">
                
                <div class="form-group">
                    <label for="type" class="col-sm-6">Rechercher parmi les</label>
                    <form:select class="col-sm-6 form-control" path="type" id="type">
                        <form:option value="team">Equipes</form:option>
                        <form:option value="tournament">Tournois</form:option>
                    </form:select>
                </div> 
                
                <div id="teamSpecific">
                    <div class="form-group">
                        <label for="captain" class="col-sm-6">Etant dirig�e par</label>
                        <form:select class="col-sm-6 form-control" path="captain" id="captain">
                            <form:option value="0">--</form:option>
                            <c:forEach items="${captains}" var="player">
                                <form:option value="${player.userId}">${player.username}</form:option>
                            </c:forEach>
                        </form:select>
                    </div>                
                    <div class="form-group">
                        <label for="player" class="col-sm-6">Contenant le joueur</label>
                        <form:select class="col-sm-6 form-control" path="player" id="player">
                            <form:option value="0">--</form:option>
                            <c:forEach items="${players}" var="player">
                                <form:option value="${player.userId}">${player.username}</form:option>
                            </c:forEach>
                        </form:select>
                    </div>
                </div>
                
                <div id="tournamentSpecific">
                    <div class="form-group">
                        <label for="organiser" class="col-sm-6">Etant organis� par</label>
                        <form:select class="col-sm-6 form-control" path="organiser" id="organiser">
                            <form:option value="0">--</form:option>
                            <c:forEach items="${organisers}" var="player">
                                <form:option value="${player.userId}">${player.username}</form:option>
                            </c:forEach>
                        </form:select>
                    </div>                
                    <div class="form-group">
                        <label for="game" class="col-sm-6">Sur le jeu</label>
                        <form:select class="col-sm-6 form-control" path="game" id="game">
                            <form:option value="0">--</form:option>
                            <c:forEach items="${games}" var="game">
                                <form:option value="${game.gameId}">${game.name}</form:option>
                            </c:forEach>
                        </form:select>
                    </div>
                </div>
                    
                <button type="submit" class="btn btn-primary">Rechercher</button>
            </form:form>
        </div>
            
        </div>
    </body>
</html>
