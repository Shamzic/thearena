<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bienvenue sur The Arena</title>
        <%@ include file="header.jsp" %>
        <%@ include file="menu.jsp" %>

    </head>

    <body>
        <div id="titre" class="alert alert-secondary" role="alert">
              Bienvenue sur The Arena
        </div>
        <%@ include file="listeUsers.jsp" %>


        <%@include file="footer.jsp" %>
    </body>
</html>