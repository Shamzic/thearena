<%-- 
    Document   : tournoisDetail
    Created on : 25 oct. 2018, 18:00:50
    Author     : Jeanba
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="dateValue" class="java.util.Date" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <%@ include file="header.jsp" %>
    <title><c:out value="${tournoi.name}">Cr�ation de votre tournoi</c:out></title>
    </head>
    <body class="h-100">
    <%@ include file="menu.jsp" %>

    <div class="container main-container pt-3 pb-3 h-100">
        <c:if test="${!empty param.errorMsg || !empty errorMsg}">
            <div class="alert alert-danger" role="alert">
                ${param.errorMsg} ${errorMsg}
            </div>
        </c:if>
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <c:if test="${!empty tournoi}">
                <li class="nav-item">
                    <a class="nav-link active" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true">General</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" id="equipes-tab" data-toggle="tab" href="#equipes" role="tab" aria-controls="equipes" aria-selected="false">Equipes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="tags-tab" data-toggle="tab" href="#tags" role="tab" aria-controls="tags" aria-selected="false">Tags</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" id="match-tab" data-toggle="tab" href="#matchs" role="tab" aria-controls="matchs" aria-selected="false">Matchs</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="logs-tab" data-toggle="tab" href="#logs" role="tab" aria-controls="logs" aria-selected="false">Logs</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="logs-tab" data-toggle="tab" href="#delete" role="tab" aria-controls="delete" aria-selected="false">Supprimer</a>
                </li>
            </c:if>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade active show" id="general" role="tabpanel" aria-labelledby="general-tab">
                <form:form method="POST">
                    <input type="hidden" name="geek.userId" value="${tournoi.geek.userId}"/>
                    <input type="hidden" name="periodByPlayingPeriod.periodId" value="${tournoi.periodByPlayingPeriod.periodId}"/>
                    <input type="hidden" name="periodByRegisteringPeriod.periodId" value="${tournoi.periodByRegisteringPeriod.periodId}"/>
                    <div class="row">
                        <c:if test="${empty tournoi}">
                            <div class="form-group col-3">
                                <label for="tournamentId">ID du tournoi</label><br>
                                <input type="text" class="form-control" id="tournamentId" name="tournamentId" value="${tournoi.name}" placeholder="Enter ID" />
                            </div>
                        </c:if>
                        <div class="form-group col-8">
                            <label for="name">Nom du tournoi</label><br>
                            <input type="text" class="form-control" id="name" name="name" value="${tournoi.name}" placeholder="Enter name" required/>
                        </div>
                    </div>
                    <div class="dropdown-divider"></div>
                    <label for="rules"><h5>R�gles</h5></label>
                    <textarea type="text" class="form-control" name="rules" id="rules" placeholder="Enter name"><c:out value="${tournoi.rules}"></c:out></textarea>  
                        <div class="dropdown-divider"></div>

                    <c:choose>
                        <c:when test="${!empty games}">
                            <h5><span class="fa fa-gamepad"></span> Jeu jou� au tournoi</h5>
                            <select class="form-control form-control-sm" name="game.gameId">
                                <c:forEach items="${games}" var="game">
                                    <c:if test="${!game.deleted}">
                                        <option value="${game.gameId}">${game.name}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                            <div class="dropdown-divider"></div>
                        </c:when>
                        <c:otherwise>
                            <input type="hidden" name="game.gameId" value="${tournoi.game.gameId}"/>
                        </c:otherwise>
                    </c:choose>       
                    <h5 class="card-title">Param�tre des �quipes</h5>
                    <div class="form-inline">
                        <div class="form-group">
                            <label id="playerNumber">Taille des �quipes :</label> 
                            <input type="number" class="form-control m-1" id="playerNumber" name="playerNumber" value="${tournoi.playerNumber}" placeholder="Nombre joueur" required>
                        </div>
                        <div class="form-group">
                            <label id="slots">Nombre d'�quipes :</label>
                            <input type="number" class="form-control m-1" id="slots" name="slots" value="${tournoi.slots}" placeholder="Enter name">
                        </div>
                    </div>
                    <h4><span class="fa fa-clock"></span> P�riode d'inscription</h4> 
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="registeringPeriodStart">D�bute le</label>
                            <input type="date" class="form-control" name="registeringPeriodStart" id="registeringPeriodStart" value="<fmt:formatDate value="${registrationPeriod.beginDate}" pattern="yyyy-MM-dd"/>" placeholder="DateDebut">
                        </div>
                        <div class="form-group">
                            <label for="registeringPeriodEnd">Termine le</label>
                            <input type="date" class="form-control" name="registeringPeriodEnd" id="registeringPeriodEnd" value="<fmt:formatDate value="${registrationPeriod.endDate}" pattern="yyyy-MM-dd"/>" placeholder="DateFin">
                        </div>
                    </div>   
                    <h4><span class="fa fa-clock"></span> P�riode de jeux</h4>
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="playingPeriodStart">D�bute le</label>
                            <input type="date" class="form-control m-1" id="playingPeriodStart" name="playingPeriodStart" value="<fmt:formatDate value="${playingPeriod.beginDate}" pattern="yyyy-MM-dd"/>" placeholder="DateDebut">

                        </div>
                        <div class="form-group">
                            <label for="playingPeriodEnd">Termine le</label>
                            <input type="date" class="form-control m-1" id="playingPeriodEnd" name="playingPeriodEnd" value="<fmt:formatDate value="${playingPeriod.endDate}" pattern="yyyy-MM-dd"/>" placeholder="DateFin">
                        </div>
                    </div>      
                    <c:if test="${empty tournoi}">
                        <div class="dropdown-divider"></div>
                        <h4>Tags</h4>
                        <label for="tags">Ins�rez les tags de votre tournoi en les s�parant d'une virgule.</label>
                        <input type="text" id="tags" name="tags" class="form-control" />
                    </c:if>
                    <p class="text-right"><button type="submit" class="btn btn-primary">Submit</button></p>

                </form:form>
            </div>
            <div class="tab-pane fade" id="equipes" role="tabpanel" aria-labelledby="profile-tab">
                <h3>Equipes participantes : </h3>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Nom d'�quipe</th>
                            <th scope="col">Place</th>
                            <th scope="col">Qualifi�</th>
                            <th scope="col">Modifier</th>
                            <th scope="col">Disqualifier</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${tournoi.participations}" var="participation">
                            <c:if test="${!participation.deleted}">
                                <tr>
                                    <form:form action="updateParticipation" method="POST">
                                <input type="hidden" value="${participation.team.teamId}" name="team.teamId" />
                                <th scope="row">
                                    <c:out value="${participation.team.name}"></c:out>
                                    </th>
                                    <td>
                                        <input type="number" name="place" class="form-control" value="<c:out value="${participation.place}"></c:out>">                                    
                                    </td>
                                    <td>
                                        <input type="checkbox" name="qualified"<c:if test="${participation.qualified}">checked</c:if> />
                                    </td>
                                    <td>
                                        <input type="submit" class="btn btn-outline-success" value="Modifier">
                                    </td>
                            </form:form> 
                            <td>
                                <form:form method="POST" action="leave" class="form-inline">
                                    <input type="hidden" name="teamId" value="${participation.team.teamId}" />
                                    <input type="submit" class="btn btn-outline-danger" value="Abandonner" />
                                </form:form>
                            </td>
                            </tr>  
                        </c:if>

                    </c:forEach>
                    </tbody>
                </table>

            </div>
            <div class="tab-pane fade" id="logs" role="tabpanel" aria-labelledby="logs-tab">
                <h1>Logs de ce tournoi :</h1>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col-lg-3">Date</th>
                            <th scope="col">Message</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${tournoi.tournamentlogs}" var="log">
                            <jsp:setProperty name="dateValue" property="time" value="${log.time *1000}" />
                            <tr>
                                <th scope="row">${log.entryId}</th>
                                <td>${dateValue}</td>
                                <td>${log.entry}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="tags" role="tabpanel" aria-labelledby="tags-tab">
                <c:forEach items="${tournoi.tournamenttags}" var="tag">
                    <c:if test="${!tag.deleted}">      
                        <form:form method="POST" action="deleteTag" class="form-inline">
                            <p>${tag.tag}</p>
                            <input type="hidden" name="tagId" value="${tag.tagId}" />
                            <input type="submit" class="btn btn-outline-danger" value="Supprimer" />
                        </form:form>
                    </c:if>
                </c:forEach>

                <form:form class="form-inline" method="POST" action="addTag">
                    <div class="input-group">
                        <label for="addTag">Nom du tag � ajouter</label>
                        <input type="text" name="tag" id="addTag" class="form-control"/> 
                    </div>
                    <input type="submit" class="btn btn-outline-success" value="Ajouter" />
                </form:form>
            </div>
            <div class="tab-pane fade" id="delete" role="tabpanel" aria-labelledby="delete-tab">
                Attention, supprimer le tournoi est une action irr�versible, �tes vous s�r de vouloir faire �a ?
                <a href="delete"><button type="button" class="btn btn-outline-danger">Supprimer</button></a>
            </div>
            <%@ include file="footer.jsp" %>
            </body>
            </html>
