<%-- 
    Document   : tournoisDetail
    Created on : 25 oct. 2018, 18:00:50
    Author     : Jeanba
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="dateValue" class="java.util.Date" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="header.jsp" %>
        <title>${tournoi.name}</title>
    </head>
    <body class="h-100">
    <%@ include file="menu.jsp" %>
    <div class="container main-container pt-3 pb-3 h-100">
        <c:if test="${!empty param.errorMsg || !empty errorMsg}">
            <div class="alert alert-danger" role="alert">
                ${param.errorMsg} ${errorMsg}
            </div>
        </c:if>
        <div class="row">
            <div class="col-12">
                <div class="">
                    <h1 class="text-center">${tournoi.name} 
                        <c:choose>
                            <c:when test="${tournoi.geek.surname == pageContext.request.userPrincipal.principal.databaseObject.surname}" >
                                <a href="${tournoi.tournamentId}/edit"><button type="button" class="btn btn-outline-success"><span class="fas fa-cog"></span></button></a>
                                
                            </c:when>
                            <c:when test="${not empty pageContext.request.userPrincipal.name}">
                                
                                <c:set var="canAdd" value="true" />
                                <c:set var="haveTeams" value="false" />
                                <c:set var="canLeave" value="" />
                                
                                <c:forEach var="teamGeek" items="${pageContext.request.userPrincipal.principal.databaseObject.teamGeeks}">
                                    <c:set var="haveTeams" value="true" />
                                    <c:forEach var="participation" items="${tournoi.participations}">
                                        <c:if test="${teamGeek.team.teamId == participation.team.teamId && !teamGeek.deleted && !participation.deleted}">
                                            <c:set var="canAdd" value="false" />
                                            <c:if test="${teamGeek.team.geek.username eq pageContext.request.userPrincipal.principal.databaseObject.username}">
                                                <c:set var="canLeave" value="${teamGeek.team.teamId}" />
                                            </c:if>
                                        </c:if>
                                    </c:forEach>
                                </c:forEach>
                                
                                <c:if test="${playingPeriod.beginDate < dateValue && playingPeriod.endDate > dateValue}">
                                    <c:set var="canAdd" value="false" />
                                </c:if>
                                <c:if test="${canAdd && haveTeams}">
                                    <form:form method="POST" action="${tournoi.tournamentId}/participate" class="form-inline">
                                        <select class="form-control" name="teamId">
                                            <c:forEach items="${pageContext.request.userPrincipal.principal.databaseObject.teams}" var="team">
                                                <option value="${team.teamId}">${team.name}</option>
                                            </c:forEach>
                                        </select>
                                        <input type="submit" class="btn btn-outline-success" value="Participer" />
                                    </form:form>
                                </c:if>
                                <c:if test="${!empty canLeave}">
                                    <form:form method="POST" action="${tournoi.tournamentId}/leave" class="form-inline">
                                        <input type="hidden" name="teamId" value="${canLeave}" />
                                        <input type="submit" class="btn btn-outline-danger" value="Abandonner" />
                                    </form:form>
                                    
                                </c:if>
                                 
                                
                            </c:when>
                        </c:choose>
                    </h1>
                    <h6 class="text-center">Organis� par <b>${tournoi.geek.name}</b></h6>
                    <h6 class="text-center"><c:if test="${playingPeriod.beginDate < dateValue && playingPeriod.endDate > dateValue}"><span class="badge badge-success">Tournoi en cours</span></c:if> </h6>
                    <h6 class="text-center"><c:if test="${playingPeriod.endDate < dateValue}"><span class="badge badge-danger">Tournoi termin�</span></c:if> </h6>
                    <div class="dropdown-divider"></div>
                </div>
                <div class="row justify-content-between">
                    <div class="col-5 text-center">
                        <p class="m-0"><small>D�bute le</small></p>
                        <p><span class="fa fa-clock"></span> <fmt:formatDate value="${playingPeriod.beginDate}" pattern="E dd MMMM yyyy"/></p>
                    </div>
                    <div class="col-5 text-center">
                        <p class="m-0"><small>Termine le</small></p>
                         <p><span class="fa fa-clock"></span> <fmt:formatDate value="${playingPeriod.endDate}" pattern="E dd MMMM yyyy"/></p>
                    </div>                  
                </div>
            </div>
        </div>
        <div class="row">
            
            <div class="pt-lg-5 col-lg-3 text-center">
                <h3> Jeu s�lectionn� :</h3>
                <p><c:out value="${tournoi.game.name}"></c:out></p>
                <h4> Type de jeu :</h4>
                <p><c:out value="${tournoi.game.gametype.name}"></c:out></p>
                <h4> Limite d'�ge :</h4>
                <p><c:out value="${tournoi.game.ageLimit}"></c:out></p>          
            </div>
            <div class="pl-3 col-lg-9">
                <div>
                    <h5>Description</h5>
                    <c:out value="${tournoi.rules}"></c:out>
                    <h5 class="card-title">Param�tre des �quipes</h5>
                    <p class="card-text">Taille des �quipes : <b>${tournoi.playerNumber}</b></p>
                    <p class="card-text">Nombre d'�quipes : <b>${tournoi.slots}</b></p>
                    <p class="card-text">P�riode d'inscription : <b><fmt:formatDate value="${registrationPeriod.beginDate}" pattern="E dd MMMM yyyy"/></b> au 
                        <b><fmt:formatDate value="${registrationPeriod.endDate}" pattern="E dd MMMM yyyy"/></b><h6 class="text-center">
                        <c:if test="${registrationPeriod.beginDate < dateValue && registrationPeriod.endDate > dateValue}"><span class="badge badge-success">Inscription ouvertes</span></c:if> </h6></p>
                    
                    <h3>Equipes participantes : </h3>
                    <c:forEach items="${tournoi.participations}" var="participation">
                        <c:if test="${!participation.deleted}">
                            <div class="card-body">
                                <h5 class="card-title"><a href="../equipe/${participation.team.teamId}"><c:out value="${participation.team.name}"></c:out></a></h5>
                            </div>  
                        </c:if>
                                
                    </c:forEach>
                </div>
            
            </div>
            
        </div>
    </div>
    </body>
</html>
