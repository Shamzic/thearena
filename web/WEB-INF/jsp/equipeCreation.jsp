<%-- 
    Document   : equipeCreation
    Created on : 26 oct. 2018, 17:42:30
    Author     : nhuch
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="header.jsp" %>
        <title>Cr�ation �quipe</title>
    </head>
    <body class="h-100">
    <%@ include file="menu.jsp" %>
    <form:form method="post">
        
    <div class="container main-container pt-3 pb-3 h-100">
        <div class="row">
            <div class="col-12">
                <div class="">
                    <c:if test="${!empty param.errorMsg || !empty errorMsg}">
                        <div class="alert alert-danger" role="alert">
                            ${param.errorMsg} ${errorMsg}
                        </div>
                    </c:if>
                    <h1 class="text-center"><input type="text" class="form-control" name="name" value="${equipe.name}" placeholder="Enter name"></h1>
                </div>
                <div class="">
                    <h4>Tags</h4>
                    <label for="tags">Ins�rez les tags de votre tournoi en les s�parant d'une virgule.</label>
                    <input type="text" id="tags" name="tags" class="form-control" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="pl-3 col-lg-1 offset-10">
                <div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
    </form:form>
    </body>
</html>
