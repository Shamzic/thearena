
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="header.jsp" %>
        <title>Profil du Geek</title>
    </head>
    <body class="h-100">
                <%@ include file="menu.jsp" %>
    <div class="container main-container h-100">
        <h3>Profil du Geek</h3>
            <c:forEach items="${geek}" var="geek">
                <c:if test="${pageContext.request.userPrincipal.name eq geek.username}">
                    <p>Name:  ${geek.name}</p>
                    <p>Surname:  ${geek.surname}</p>
                    <p>Username:  ${geek.username}</p>
                    <p>Mail:  ${geek.mail}</p>
                    <p>Birth Date:  ${geek.birthdate}</p>
                    <a href="geek/${geek.username}/edit" class="card-link">Editer le profil</a>
                </c:if>
            </c:forEach>
        
    </div>
    </body>
        <%@ include file="footer.jsp" %>

</html>
