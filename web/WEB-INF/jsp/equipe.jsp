<%-- 
    Document   : equipe
    Created on : 22 oct. 2018, 17:29:58
    Author     : nhuc
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="header.jsp" %>
        <title>Equipes</title>
    </head>
    <body class="h-100">
    <%@ include file="menu.jsp" %>
    <div class="container main-container pt-3 pb-3 h-100">
        <h3>Liste des �quipes inscrite : <c:if test="${not empty pageContext.request.userPrincipal.name}"><a href="equipe/create"><button type="button" class="btn btn-outline-success">Creer votre �quipe</button></a></c:if></h3>
        <div class="row">
        <c:forEach items="${equipe}" var="equipe">
            <div class="card col-lg-3" style="width: 18rem;">
                <div class="card-body">
                  <h5 class="card-title">${equipe.name}</h5>
                  <h6 class="card-subtitle mb-2 text-muted">${equipe.tags}</h6>
                  <p class="card-text">Capitaine : ${equipe.geek.username}</p>
                  <a href="equipe/${equipe.teamId}" class="card-link">Consulter</a>
                  <c:if test="${pageContext.request.userPrincipal.name eq equipe.geek.username}"><a href="equipe/${equipe.teamId}/edit" class="card-link">Modifier</a></c:if>
                </div>
              </div>
        </c:forEach>
        </div>
    </div>
    </body>
</html>
