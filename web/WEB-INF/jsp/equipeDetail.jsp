<%-- 
    Document   : equipeDetail
    Created on : 26 oct. 2018, 12:27:36
    Author     : nhuch
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="header.jsp" %>
        <title>${equipe.name}</title>
    </head>
    <body class="h-100">
        <c:set var="contains" value="false" />
        <c:forEach var="membre" items="${membres}">
            <c:if test="${membre.geek.username eq pageContext.request.userPrincipal.name && !membre.deleted}">
                <c:set var="contains" value="true" />
            </c:if>
        </c:forEach>
    <%@ include file="menu.jsp" %>
        <div class="container main-container pt-3 pb-3 h-100">
        <div class="row">
            <div class="col-12">
                <div class="">
                    <c:if test="${!empty param.errorMsg || !empty errorMsg}">
                        <div class="alert alert-danger" role="alert">
                            ${param.errorMsg} ${errorMsg}
                        </div>
                    </c:if>
                    <h1 class="text-center">${equipe.name} 
                        <c:if test="${not empty pageContext.request.userPrincipal.name}">
                            <a href="${equipe.teamId}/edit"><c:if test="${pageContext.request.userPrincipal.name eq equipe.geek.username}"><button type="button" class="btn btn-outline-success">Modifier</button></c:if></a>
                        </c:if>
                    </h1>
                    <h6 class="text-center">Capitaine <b>${equipe.geek.username}</b></h6>
                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="pt-lg-4 col-lg-3 text-center">
                <div class="card-body">
                   <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Membres</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${membres}" var="membre">
                            <c:if test="${!membre.deleted}">
                                <tr>
                                <th scope="row">
                                    <c:out value="${membre.geek.username}"></c:out>
                                    </th>  
                                </tr>  
                            </c:if>
                        </c:forEach>
                    </tbody>
                </table>
                </div>
            </div>
            <div class="pt-lg-2 col-lg-6 text-center">
                <div class="card-body">
                   <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Participation Tournois</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:set var="condition" value="true" />
                        <c:forEach items="${tournois}" var="tournois">
                            <c:if test="${!tournois.tournament.deleted and !tournois.deleted}">
                                <c:set var="condition" value="false" />
                                <tr>
                                <th scope="row">
                                   <a href="../tournois/${tournois.tournament.tournamentId}" class="card-link">${tournois.tournament.name}</a>
                                    </th>
                                </tr>  
                            </c:if>
                        </c:forEach>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 text-center offset-10">
                <c:if test="${not empty pageContext.request.userPrincipal.name}">
                    <form:form action="${equipe.teamId}/delete" method="Post" ><c:if test="${pageContext.request.userPrincipal.name eq equipe.geek.username}"><input type="submit"class="btn btn-outline-danger" value="Supprimer"></c:if></form:form>
                </c:if>
                    <c:if test="${not empty pageContext.request.userPrincipal.name}">
                        <c:if test="${pageContext.request.userPrincipal.principal.databaseObject.userId != equipe.geek.userId}">
                        <form:form methode="POST" action="${equipe.teamId}/join" class="form-inline">
                            <c:if test="${!contains && condition}">
                                <input type="hidden" name="userId" value="${pageContext.request.userPrincipal.principal.databaseObject.userId}"/>
                                <input type="submit" class="btn btn-outline-success" value="Rejoindre">
                            </c:if>
                        </form:form>
                        <form:form action="${equipe.teamId}/leave" method="Post" class="form-inline">
                            <c:if test="${contains && condition}">
                                <input type="hidden" name="userId" value="${pageContext.request.userPrincipal.principal.databaseObject.userId}"/>
                                <input type="Submit" class="btn btn-outline-danger" value="Quitter">
                            </c:if>
                        </form:form>
                        </c:if>
                    </c:if>
            </div>
        </div>        
    </div>
                
    </body>
</html>
