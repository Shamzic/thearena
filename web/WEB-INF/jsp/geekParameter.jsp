<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE HTML>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="header.jsp" %>
        <title>Parametre Profil</title>
    </head>
    <body class="h-100">
                <%@ include file="menu.jsp" %>
                <form:form method="post" action="/TheArena/geek/${geek.username}/edit">
        <div class="container main-container h-100">
            <input type="hidden" id="user" name="userId" value="${geek.userId}">
            <p>Name :<h1 class="main-container"> <input type="text" class="form-control" name="name" value="${geek.name}" placeholder="${geek.name}"></h1></p>
            <p>Surname :<h1 class="main-container"> <input type="text" class="form-control" name="surname" value="${geek.surname}" placeholder="${geek.surname}"></h1></p> 
           
            <!-- ATTENTION On ne peut pas modifier le USERNAME car c'est la clef primaire de la table Geek !
             De plus, bien v�rifier l'attribut name=" " different pour chaque champ car il est source de conflits-->

            <p>Password :<h1 class="main-container"><input type="text" class="form-control" name="password" value="${geek.password}" placeholder="${geek.password}"></h1></p> 
            <p>Mail :<h1 class="main-container"><input type="text" class="form-control" name="mail" value="${geek.mail}" placeholder="${geek.mail}"></h1></p> 
            <p>Birthdate :<h1 class="main-container"> <input type="number" class="form-control" name="birthdate" value="${geek.birthdate}" placeholder="${geek.birthdate}"></h1></p> 
            <button type="submit" class="btn btn-primary">Submit</button>

</div>
    </form:form>  

    
    </body>
    
    <%@ include file="footer.jsp" %>
</html>

