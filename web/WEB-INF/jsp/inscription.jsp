<%-- 
    Document   : inscription
    Created on : 24 oct. 2018, 23:03:48
    Author     : shame
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="header.jsp" %>
        <title>Inscription utilisateur</title>
        <link type="text/css" rel="stylesheet" href="ressources/CSS/form.css" />
    </head>
    <body class="h-100">

    <%@ include file="menu.jsp" %>
    <form:form method="post" action="inscription" >
             <fieldset class="formulaire-inscription">
                <legend>Inscription</legend>
                <p>Vous pouvez vous inscrire via ce formulaire.</p>

                 <label for="mail">Adresse email <span class="requis">*</span></label>
                <input type="email" id="mail" name="mail" value="${geek.mail}" size="20" maxlength="60"  placeholder="your@email.com" />
                <span class="erreur">${form.erreurs['email']}</span>
                <br />

                <label for="motdepasse">Mot de passe <span class="requis">*</span></label>
                <input type="password" id="password" name="password" value="${geek.password}" size="20" maxlength="20" />

                <br />

                <label for="pass">Confirmation du mot de passe <span class="requis">*</span></label>
                <input type="password" id="pass" name="pass" value="${pass}" size="20" maxlength="20" />
                <div style="color: red;"> <c:out value="${sessionScope.echecPassword}" /></div>
                <br />
                
                <label for="username">Nom d'utilisateur</label>
                <input type="text" id="username" name="username" value="${geek.username}" size="20" maxlength="20" />
                <div style="color: red;"> <c:out value="${sessionScope.echecUserName}" /></div>
                <br />
                
                 <label for="name">Nom</label>
                <input type="text" id="name" name="name" value="${geek.name}" size="20" maxlength="20" />
                <br />
                <label for="surname">Surnom</label>
                <input type="text" id="surname" name="surname" value="${geek.surname}" size="20" maxlength="20" />
                 <br />
                <label for="username">Date de naissance</label>

                <input type="number" placeholder="JJMMAAAA" id="birthdate" name="birthdate" value="${geek.birthdate}" size="20" maxlength="20"/>

                <br />

                <input type="submit" value="Inscription" class="sansLabel" />
                <br />
                
                <p style="color: red;"> <c:out value="${sessionScope.echecInscription}" /></p>
                <p style="color: green;"> <c:out value="${sessionScope.succesInscription}" /></p>
            </fieldset>
        </form:form>
    </body>
</html>