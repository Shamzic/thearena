<%-- 
    Document   : advancedSearchResult
    Created on : 1 nov. 2018, 09:31:42
    Author     : ASUS PC
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="header.jsp" %>
        <title>Recherche avanc�e</title>
    </head>
    <body>
        <%@ include file="menu.jsp" %>
        <div class="container main-container pt-3 pb-3 h-100">
        <div class = "col-sm-12">
            
            <p style="color:gray;">
                <b>R�sultats de la recherche parmis les  
                <c:if test="${type == 'team'}">�quipes</c:if>
                <c:if test="${type == 'tournament'}">tournois</c:if>
                 :</b>
            </p>
            
            <c:if test = "${empty results}">
                <p>Aucun r�sultat n'a �t� trouv�, veuillez r�essayer avec de nouveaux crit�res de recherche.
            </c:if>
            <c:if test = "${!empty results}">
                <ul>
                    <c:if test="${type == 'team'}">
                        <c:forEach items="${results}" var="result">
                            <li><a href="equipe/${result}">${teamDAO.getTeamById(result).name}</a></li>
                        </c:forEach> 
                    </c:if>

                    <c:if test="${type == 'tournament'}">
                        <c:forEach items="${results}" var="result">
                            <li><a href="tournois/${result}">${tournamentDAO.getTournamentById(result).name}</a></li>
                        </c:forEach> 
                    </c:if>
                </ul>
            </c:if>
            
        </div>
        </div>
        
    </body>
</html>
