<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <nav class="navbar navbar-expand-lg navbar-ta-main">
    <a class="navbar-brand" href="/TheArena">The <span class="navbar-ta-main-a">A</span>rena <span class="fas fa-gamepad"></span> </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <div class="container">
            <div class="row">
                <form class="form-inline input-group my-2 my-lg-0 col-lg-5" method="GET" action="${pageContext.request.contextPath}/search">
                    <input class="form-control mr-sm-2 search" type="search" placeholder="Rechercher" aria-label="Search" name="searchTerms">

                    <div class="input-group-append ">
                        <button class="btn btn-outline-success search-button my-0" type="submit"><span class="fas fa-search"></span> </button>
                    </div>
                </form>
                <a class="nav-link" href="${pageContext.request.contextPath}/advancedSearch">Recherche avanc�e</a>
                <ul class="nav col justify-content-end">
                    <c:choose>
                        <c:when test="${not empty pageContext.request.userPrincipal.name}">
                            <li id ="username" class="nav-item">Bienvenue <c:out value="${pageContext.request.userPrincipal.name}"/>  </li>
                            <form:form class="form-inline my-2 my-lg-0" method="POST" action="${pageContext.request.contextPath}/logout">
                                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Se d�connecter</button>
                             </form:form>
                        </c:when>
                        <c:otherwise>
                            <li>
                                <form class="form-inline my-2 my-lg-0" method="GET" action="${pageContext.request.contextPath}/login.html">
                                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Se connecter</button>
                                </form>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/TheArena/inscription">S'enregistrer</a>
                            </li>
                        </c:otherwise>
                    </c:choose>                    
                </ul>
            </div>
        </div>
    </div>
</nav>
<nav class="navbar navbar-expand-lg navbar-ta-menu">
    <div class="collapse navbar-collapse container justify-content-center" id="navbarMenuSupportedContent">
        <div class="row align-items-center ">
            <ul class="nav nav-fill align-middle">
                <li class="nav-item">
                    <a href="/TheArena/index.htm" class="nav-link"><span class="fas fa-home"></span>Accueil</a>
                </li>
                <li class="nav-item">
                    <a href="/TheArena/tournois" class="nav-link"><span class="fas fa-calendar"></span> Tournois</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/TheArena/equipe"><span class="fas fa-users"></span>Joueurs/Equipes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><span class="fas fa-gamepad"></span> Jeux</a>
                </li>
                <c:if test="${not empty pageContext.request.userPrincipal.name}">
                <li class="nav-item">
                    <a class="nav-link" href="/TheArena/geek"><span class="fas fa-user-circle"></span> Profil</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/TheArena/geek/${pageContext.request.userPrincipal.name}/edit"><span class="fas fa-cog"></span> Param�tres</a>
                </li>
                </c:if>
            </ul>
        </div>
    </div>
</nav>