<%-- 
    Document   : equipeEdition
    Created on : 26 oct. 2018, 13:44:42
    Author     : nhuch
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="header.jsp" %>
        <title>Equipes</title>
    </head>
    <body class="h-100">
    <%@ include file="menu.jsp" %>
    
    <div class="container main-container pt-3 pb-3 h-50">
        <form:form method="post">
        <div class="row">
            <div class="col-12">
                <div class="">
                    <c:if test="${!empty param.errorMsg || !empty errorMsg}">
                        <div class="alert alert-danger" role="alert">
                            ${param.errorMsg} ${errorMsg}
                        </div>
                    </c:if>
                    <h1 class="text-center"><input type="text" class="form-control" name="name" value="${equipe.name}" placeholder="Enter name"><button type="submit" class="btn btn-primary">Submit</button></h1>
                    <h6 class="text-center">Capitaine <b>${equipe.geek.username}</b></h6>              
                </div>
            </div>
        </div>
        </form:form>
        <div class="row">
            <div class="pt-lg-4 col-lg-4 text-center">
                <div class="card-body">
                   <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Membres</th>
                            <th scope="col">Exclure</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${membres}" var="membre">
                            <c:if test="${!membre.deleted}">
                                <tr>
                                <th scope="row">
                                    <c:out value="${membre.geek.username}"></c:out>
                                    </th> 
                                    <td>
                                        <c:if test="${!(pageContext.request.userPrincipal.name eq membre.geek.username)}">
                                            <form:form action="leave" method="POST">
                                                <input type="hidden" name="userId" value="${membre.geek.userId}" />
                                                <input type="submit" class="btn btn-outline-danger" value="Exclure" />
                                            </form:form>
                                        </c:if>
                                    </td>
                                </tr>  
                            </c:if>
       
                        </c:forEach>
                    </tbody>
                </table>
                </div>
            </div>
            <div class="pt-lg-2 col-lg-4 text-center">
                <div class="card-body">
                   <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Participation Tournois</th>
                            <th scope="col">Abandon</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${tournois}" var="tournois">
                            <c:if test="${!tournois.tournament.deleted and !tournois.deleted}">
                                <tr>
                                <th scope="row">
                                   <a href="../tournois/${tournois.tournament.tournamentId}" class="card-link">${tournois.tournament.name}</a>
                                    </th>
                                    <th scope="row">
                                        <form:form action="ff" method="POST">
                                    <input type="hidden" name="tournamentId" value="${tournois.tournament.tournamentId}" />
                                    <input type="submit" class="btn btn-outline-danger" value="Abandonner" />
                                  </form:form>
                                    </th>
                                </tr>  
                            </c:if>
                        </c:forEach>
                    </tbody>
                </table>
                </div>
            </div>
            <div class="pt-lg-4 col-lg-4 text-center">
                <div class="card-body">
                    <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Tags</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${equipe.teamtags}" var="tag">
                            <c:if test="${!tag.deleted}">
                                <tr>
                                <th scope="row">
                                    <c:out value="${tag.tag}"></c:out>
                                    </th>
                                    <th scope="row">
                                <form:form action="suppr" method="POST">
                                    <input type="hidden" name="tagId" value="${tag.tagId}" />
                                    <input type="submit" class="btn btn-outline-danger" value="Supprimer" />
                                 </form:form>
                                    </th>
                                </tr>  
                            </c:if>
                        </c:forEach>
                                <tr>
                                 <form:form action="add" method="POST">
                                     <th>
                                        <input type="text" class="form-control" name="tag" placeholder="Ajouter un Tag">
                                     </th>
                                     <th>
                                        <input type="submit" class="btn btn-outline-success" value="ajouter" />
                                     </th>
                                 </form:form>
                                </tr>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div> 
    
    </body>
</html>
