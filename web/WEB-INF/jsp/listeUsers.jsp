<%-- 
    Document   : listejoueurs
    Created on : 31 oct. 2018, 20:24:38
    Author     : shame
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

  <div class="container">
    <div class="row">
      <div id="liste" class="col-sm alert alert-secondary" role="alert">
            <h3>Utilisateurs enregistrés récemment :</h3>
              <ul >
                  <c:forEach items="${geeks}" var="geek" >
                      <li>
                      ${geek.username}
                       </li>  
                  </c:forEach> 
              </ul>
      </div>
      <div id="liste" class="col-sm alert alert-secondary" role="alert">
            <h3>Tournois récemment programmés :</h3>
              <ul>
                  <c:forEach items="${tournois}" var="tournoi">
                      <li>
                      ${tournoi.name}
                       </li>  
                  </c:forEach> 
              </ul>
      </div>
    </div>
</div>
