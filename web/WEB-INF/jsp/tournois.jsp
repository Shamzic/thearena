<%-- 
    Document   : tournois
    Created on : 22 oct. 2018, 17:29:58
    Author     : shame
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="dateValue" class="java.util.Date" />
<c:set var="timestampNow" value="${dateValue.time/1000}" />
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <%@ include file="header.jsp" %>
    <title>Tournois</title>
</head>
<body class="h-100">
    <%@ include file="menu.jsp" %>
    <div class="container main-container pt-3 pb-3 h-100">
        <c:if test="${!empty param.errorMsg || !empty errorMsg}">
            <div class="alert alert-danger" role="alert">
                ${param.errorMsg} ${errorMsg}
            </div>
        </c:if>
        <h3>Tournois propos�s sur notre site : <a href="tournois/create">
                <c:if test="${not empty pageContext.request.userPrincipal.name}">
                    <button type="button" class="btn btn-outline-success">Ajoutez le votre</button>
                </c:if>
            </a></h3>
        <div class="row">
            <c:forEach items="${tournois}" var="tournoi">
                <div class="card col-lg-3 m-1" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title">${tournoi.name} 
                            <c:if test="${tournoi.periodByRegisteringPeriod.start < timestampNow && tournoi.periodByRegisteringPeriod.end > timestampNow}"><span class="badge badge-success">Inscription ouvertes</span></c:if> </h6></p></h5>
                            <c:if test="${tournoi.periodByPlayingPeriod.start < timestampNow && tournoi.periodByPlayingPeriod.end > timestampNow}"><span class="badge badge-success">Tournoi en cours</span></c:if> </h6></p></h5>
                            <c:if test="${tournoi.periodByPlayingPeriod.end < timestampNow}"><span class="badge badge-danger">Tournoi termin�</span></c:if> </h6></p></h5>
                        <h6 class="card-subtitle mb-2 text-muted">${tournoi.game.name}</h6>
                        <p class="card-text">R�gles : ${tournoi.rules}</p>
                        <a href="tournois/${tournoi.tournamentId}" class="card-link">Detail</a>
                        <c:if test="${tournoi.geek.surname == pageContext.request.userPrincipal.principal.databaseObject.surname}">
                            <a href="tournois/${tournoi.tournamentId}/edit" class="card-link">Param�trer</a>
                        </c:if>                  
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</body>
</html>
