The Arena

#Plateforme de gestion de tournois de jeux vidéo

##Outils utilisé 

Application réalisé avec Java JEE. 
Les librairies suivantes ont étés utilisé lors de la réalisation de l'application:
Hibernate v4.3.X utilisé pour le mapping des objets de la base de donnée
Spring framework 4.0.1 utilisé pour la gestion du système MVC et pour le rooting des page jsp
Spring security 4.0.1 utilisé pour gérer le système de connection de manière sécurisé.

##Environnement de test

Le serveur utilisé lors des test est un serveur Glassfish 5.0.
La base de donnée utilisée est une base de donnée MySQL et a été dévellopé afin de fonctionner avec une base de donnée MySQL.
Les test ont été réalisé sur un IDE NetBeans 8.2.

##Guide de démarrage

Il faut tout d'abord initialiser la base de donnée. Un script d'initialisation **BDD_init.sql** est disponible.

Le fichier hibernate.cfg.xml permet de paramétrer les entrées à la base de donner (mot de passe, nom de compte, nom de la table et le port utilisé).
De base le mot de passe et le nom de compte pour accèder a la base de donner sont **root** et **root** et le nom de la base de donné est **thearena**.
Le port utilisé est initialement **3306**.

Une fois le serveur lancé et la base de donnée est initialisé, l'application peut être démarré.

##Utilisation de l'application

Une fois l'application en route, vous devez vous connecter et potentiellement créer un compte avant de poursuivre.

Une fois connecté, sur la page tournois, vous pouvez créer votre propre tournois, voir les détails d'un tournois, vous inscrire à un tournois en phase d'inscription.
Sur la page équipe, vous pouvez créer votre propre équipe, accèder aux détail d'une équipe, modifier une de vos propres équipes ou rejoindre/quitter une équipe dont vous êtes membre.
Vous pouvez aussi accèder à votre page de profil et modifier des informations vous concernants. 